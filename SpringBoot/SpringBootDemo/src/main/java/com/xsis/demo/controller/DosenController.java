package com.xsis.demo.controller;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;

import com.xsis.demo.model.Dosen;
import com.xsis.demo.repository.DosenRepo;

@Controller
public class DosenController {
	// membuat auto instance dari repository
		@Autowired
		private DosenRepo repo;

		// request yang ada di url localhost:port/dosen/index
		@RequestMapping("/dosen/index")
		public String index(Model model) {
			// membuat object list biodata, kemudian diisi dari
			// object repo dengan method finAll
			List<Dosen> data = repo.findAll();
			// mengirim variable listData, value nya diisi dari objct data
			model.addAttribute("listData", data);
			// menampilkan view /src/main/resources/templates
			return "dosen/index";
		}

		// request yang ada di url localhost:port/dosen/add
		@RequestMapping("/dosen/add")
		public String add() {
			// menampilkan view /src/main/resources/templates
			return "dosen/add";
		}

		// request yang ada di url localhost:port/dosen/save
		// method nya ada post
		@RequestMapping(value = "/dosen/save", method = RequestMethod.POST)
		public String save(@ModelAttribute Dosen item) {
			// simpan kedatabase
			repo.save(item);
			// redirect: akan diteruskan ke halaman index
			return "redirect:/dosen/index";
		}

		// request edit data
		@RequestMapping(value = "/dosen/edit/{id}")
		public String edit(Model model, @PathVariable(name = "id") Integer id) {
			// mengambil data dari database dengan parameter id
			Dosen item = repo.findById(id).orElse(null);
			// mengirim data dari controller ke view
			model.addAttribute("data", item);
			return "dosen/edit";
		}

		// request edit data
		@RequestMapping(value = "/dosen/delete/{id}")
		public String hapus(@PathVariable(name = "id") Integer id) {
			// mengambil data dari database dengan parameter id
			Dosen item = repo.findById(id).orElse(null);
			// remove from database
			if (item != null) {
				repo.delete(item);
			}
			return "redirect:/dosen/index";
		}
}
