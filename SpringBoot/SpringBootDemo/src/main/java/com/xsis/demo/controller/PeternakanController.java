package com.xsis.demo.controller;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;

import com.xsis.demo.model.Peternakan;
import com.xsis.demo.repository.PeternakanRepo;

@Controller
public class PeternakanController {
	// membuat auto instance dari repository
	@Autowired
	private PeternakanRepo repo;

	// request yang ada di url localhost:port/peternakan/index
	@RequestMapping("/peternakan/index")
	public String index(Model model) {
		// membuat object list peternakan, kemudian diisi dari
		// object repo dengan method finAll
		List<Peternakan> data = repo.findAll();
		// mengirim variable listData, value nya diisi dari objct data
		model.addAttribute("listData", data);
		// menampilkan view /src/main/resources/templates
		return "peternakan/index";
	}

	// request yang ada di url localhost:port/peternakan/add
	@RequestMapping("/peternakan/add")
	public String add() {
		// menampilkan view /src/main/resources/templates
		return "peternakan/add";
	}

	// request yang ada di url localhost:port/peternakan/save
	// method nya ada post
	@RequestMapping(value = "/peternakan/save", method = RequestMethod.POST)
	public String save(@ModelAttribute Peternakan item) {
		// simpan kedatabase
		repo.save(item);
		// redirect: akan diteruskan ke halaman index
		return "redirect:/peternakan/index";
	}

	// request edit data
	@RequestMapping(value = "/peternakan/edit/{id}")
	public String edit(Model model, @PathVariable(name = "id") Integer id) {
		// mengambil data dari database dengan parameter id
		Peternakan item = repo.findById(id).orElse(null);
		// mengirim data dari controller ke view
		model.addAttribute("data", item);
		return "peternakan/edit";
	}

	// request edit data
	@RequestMapping(value = "/peternakan/delete/{id}")
	public String hapus(@PathVariable(name = "id") Integer id) {
		// mengambil data dari database dengan parameter id
		Peternakan item = repo.findById(id).orElse(null);
		// remove from database
		if (item != null) {
			repo.delete(item);
		}
		return "redirect:/peternakan/index";
	}
}
