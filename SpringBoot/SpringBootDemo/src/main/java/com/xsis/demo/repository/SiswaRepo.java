package com.xsis.demo.repository;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import com.xsis.demo.model.Siswa;

@Repository
public interface SiswaRepo extends JpaRepository<Siswa, Integer> {

}
