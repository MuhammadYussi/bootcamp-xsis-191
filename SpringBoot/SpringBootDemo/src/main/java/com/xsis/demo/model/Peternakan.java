package com.xsis.demo.model;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.Table;

@Entity
@Table(name="peternakan")
public class Peternakan {
	@Id
	@GeneratedValue(strategy = GenerationType.AUTO)
	@Column(name="id", nullable=false)
	private int id;
	
	@Column(name="jmlsapi", nullable=false)
	private int jmlsapi;
	
	@Column(name="jmlkambing", nullable=false)
	private int jmlkambing;
	
	@Column(name="jmlayam", nullable=false)
	private int jmlayam;

	public int getId() {
		return id;
	}

	public void setId(int id) {
		this.id = id;
	}
	
	public int getJmlsapi() {
		return jmlsapi;
	}

	public void setJmlsapi(int jmlsapi) {
		this.jmlsapi = jmlsapi;
	}

	public int getJmlkambing() {
		return jmlkambing;
	}

	public void setJmlkambing(int jmlkambing) {
		this.jmlkambing = jmlkambing;
	}

	public int getJmlayam() {
		return jmlayam;
	}

	public void setJmlayam(int jmlayam) {
		this.jmlayam = jmlayam;
	}
}
