package com.xsis.demo.repository;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import com.xsis.demo.model.Peternakan;

@Repository
public interface PeternakanRepo extends JpaRepository<Peternakan, Integer>{

}
