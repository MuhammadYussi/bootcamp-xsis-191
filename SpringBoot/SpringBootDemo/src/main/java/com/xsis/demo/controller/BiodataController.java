package com.xsis.demo.controller;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;

import com.xsis.demo.model.Biodata;
import com.xsis.demo.repository.BiodataRepo;

@Controller
public class BiodataController {
	// membuat auto instance dari repository
	@Autowired
	private BiodataRepo repo;

	// request yang ada di url localhost:port/biodata/index
	@RequestMapping("/biodata/index")
	public String index(Model model) {
		// membuat object list biodata, kemudian diisi dari
		// object repo dengan method finAll
		List<Biodata> data = repo.findAll();
		// mengirim variable listData, value nya diisi dari objct data
		model.addAttribute("listData", data);
		// menampilkan view /src/main/resources/templates
		return "biodata/index";
	}

	// request yang ada di url localhost:port/biodata/add
	@RequestMapping("/biodata/add")
	public String add() {
		// menampilkan view /src/main/resources/templates
		return "biodata/add";
	}

	// request yang ada di url localhost:port/biodata/save
	// method nya ada post
	@RequestMapping(value = "/biodata/save", method = RequestMethod.POST)
	public String save(@ModelAttribute Biodata item) {
		// simpan kedatabase
		repo.save(item);
		// redirect: akan diteruskan ke halaman index
		return "redirect:/biodata/index";
	}

	// request edit data
	@RequestMapping(value = "/biodata/edit/{id}")
	public String edit(Model model, @PathVariable(name = "id") Integer id) {
		// mengambil data dari database dengan parameter id
		Biodata item = repo.findById(id).orElse(null);
		// mengirim data dari controller ke view
		model.addAttribute("data", item);
		return "biodata/edit";
	}

	// request hapus data
	@RequestMapping(value = "/biodata/delete/{id}")
	public String hapus(@PathVariable(name = "id") Integer id) {
		// mengambil data dari database dengan parameter id
		Biodata item = repo.findById(id).orElse(null);
		// remove from database
		if (item != null) {
			repo.delete(item);
		}
		return "redirect:/biodata/index";
	}
}
