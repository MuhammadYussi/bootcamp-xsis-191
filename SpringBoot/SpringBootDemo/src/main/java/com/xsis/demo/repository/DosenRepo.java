package com.xsis.demo.repository;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import com.xsis.demo.model.Dosen;

@Repository
public interface DosenRepo extends JpaRepository<Dosen, Integer>{

}
