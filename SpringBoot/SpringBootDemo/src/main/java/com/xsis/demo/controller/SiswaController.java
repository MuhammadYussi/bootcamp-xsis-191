package com.xsis.demo.controller;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;

import com.xsis.demo.model.Siswa;
import com.xsis.demo.repository.SiswaRepo;

@Controller
public class SiswaController {
	
	// membuat auto instance dari repository
	@Autowired
	private SiswaRepo repo;
	
	// request yang ada di url localhost:port/siswa/index
		@RequestMapping("/siswa/index")
	public String index(Model model) {
		// membuat object list Siswa, kemudian diisi dari
		// object repo dengan method finAll
		List<Siswa> data = repo.findAll();
		// mengirim variable listData, value nya diisi dari objct data
		model.addAttribute("listData", data);
		// menampilkan view /src/main/resources/templates
		return "siswa/index";
	}
	
	// request yang ada di url localhost:port/siswa/add
	@RequestMapping("/siswa/add")
	public String add() {
		// menampilkan view /src/main/resources/templates
		return "siswa/add";
	}
	
	// request yang ada di url localhost:port/siswa/save
	// method nya ada post
	@RequestMapping(value = "/siswa/save", method = RequestMethod.POST)
	public String save(@ModelAttribute Siswa item) {
		// simpan kedatabase
		repo.save(item);
		// redirect: akan diteruskan ke halaman index
		return "redirect:/siswa/index";
	}
	
	// request edit data
	@RequestMapping(value = "/siswa/edit/{id}")
	public String edit(Model model, @PathVariable(name = "id") Integer id) {
		// mengambil data dari database dengan parameter id
		Siswa item = repo.findById(id).orElse(null);
		// mengirim data dari controller ke view
		model.addAttribute("data", item);
		return "siswa/edit";
	}
	
	// request delete data
	@RequestMapping(value = "/siswa/delete/{id}")
	public String hapus(@PathVariable(name = "id") Integer id) {
		// mengambil data dari database dengan parameter id
		Siswa item = repo.findById(id).orElse(null);
		// remove from database
		if (item != null) {
			repo.delete(item);
		}
		return "redirect:/siswa/index";
	}
}
