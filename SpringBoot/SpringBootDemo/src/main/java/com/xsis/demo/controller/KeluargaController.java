package com.xsis.demo.controller;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;

import com.xsis.demo.model.Keluarga;
import com.xsis.demo.repository.KeluargaRepo;

@Controller
public class KeluargaController {
	// membuat auto instance dari repository
	@Autowired
	private KeluargaRepo repo;
	
	// request yang ada di url localhost:port/keluarga/index
	@RequestMapping("/keluarga/index")
	public String index(Model model) {
		// membuat object list keluarga, kemudian diisi dari
		// object repo dengan method finAll
		List<Keluarga> data = repo.findAll();
		// mengirim variable listData, value nya diisi dari objct data
		model.addAttribute("listData", data);
		// menampilkan view /src/main/resources/templates
		return "keluarga/index";
	}
	
	// request yang ada di url localhost:port/keluarga/add
	@RequestMapping("/keluarga/add")
	public String add() {
		// menampilkan view /src/main/resources/templates
		return "keluarga/add";
	}
	
	// request yang ada di url localhost:port/keluarga/save
	// method nya ada post
	@RequestMapping(value = "/keluarga/save", method = RequestMethod.POST)
	public String save(@ModelAttribute Keluarga item) {
		// simpan kedatabase
		repo.save(item);
		// redirect: akan diteruskan ke halaman index
		return "redirect:/keluarga/index";
	}
	
	// request edit data
	@RequestMapping(value = "/keluarga/edit/{id}")
	public String edit(Model model, @PathVariable(name = "id") Integer id) {
		// mengambil data dari database dengan parameter id
		Keluarga item = repo.findById(id).orElse(null);
		// mengirim data dari controller ke view
		model.addAttribute("data", item);
		return "keluarga/edit";
	}
	
	// request edit data
	@RequestMapping(value = "/keluarga/delete/{id}")
	public String hapus(@PathVariable(name = "id") Integer id) {
		// mengambil data dari database dengan parameter id
		Keluarga item = repo.findById(id).orElse(null);
		// remove from database
		if (item != null) {
			repo.delete(item);
		}
		return "redirect:/keluarga/index";
	}
}