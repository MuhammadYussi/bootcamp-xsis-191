package com.xsis.api.repository;

import org.springframework.data.jpa.repository.JpaRepository;

import com.xsis.api.model.Fakultas;

public interface FakultasRepo extends JpaRepository<Fakultas, Integer>{

}
