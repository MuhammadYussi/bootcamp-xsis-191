package com.xsis.api.repository;

import org.springframework.data.jpa.repository.JpaRepository;

import com.xsis.api.model.Jurusan;

public interface JurusanRepo extends JpaRepository<Jurusan, Integer>{

}
