package com.xsis.api.controller;

import java.util.List;

import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;

import com.xsis.api.model.Fakultas;
import com.xsis.api.repository.FakultasRepo;

@Controller
public class ApiFakultasController {
	@Autowired
	private FakultasRepo repo;
	private Log log = LogFactory.getLog(getClass());
	
	@RequestMapping(value="/api/fakultas/",method=RequestMethod.GET)
	public ResponseEntity<List<Fakultas>> list(){
		ResponseEntity<List<Fakultas>> result=null;
		try {
			List<Fakultas> list=repo.findAll();
			result= new ResponseEntity<List<Fakultas>>(list,HttpStatus.INTERNAL_SERVER_ERROR);
		} catch (Exception e) {
			// TODO: handle exception
			log.debug(e.getMessage(),e);
			result = new ResponseEntity<>(HttpStatus.INTERNAL_SERVER_ERROR);
		}
		return result;
	}
	
	@RequestMapping(value="/api/fakultas/",method=RequestMethod.POST)
	public ResponseEntity<Fakultas> create(@RequestBody Fakultas item){
		ResponseEntity<Fakultas> result=null;
		try {
			repo.save(item);
			result = new ResponseEntity<Fakultas>(item,HttpStatus.CREATED);
		} catch (Exception e) {
			// TODO: handle exception
			log.debug(e.getMessage(),e);
			result = new ResponseEntity<Fakultas>(HttpStatus.INTERNAL_SERVER_ERROR);
		}
		return result;
	}
	
	@RequestMapping(value="/api/fakultas/{xyz}",method=RequestMethod.DELETE)
	public ResponseEntity<Fakultas> delete(@PathVariable(name = "xyz") Integer id){
		ResponseEntity<Fakultas> result=null;
		try {
			Fakultas item=repo.findById(id).orElse(null);
			result = new ResponseEntity<Fakultas>(item,HttpStatus.OK);
		} catch (Exception e) {
			// TODO: handle exception
			log.debug(e.getMessage(),e);
			result = new ResponseEntity<Fakultas>(HttpStatus.INTERNAL_SERVER_ERROR);
		}
		return result;
	}
}
