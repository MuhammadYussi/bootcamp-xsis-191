package com.xsis.api;

import java.util.Collections;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class NewSpringApiApplication {

	public static void main(String[] args) {
		//SpringApplication.run(NewSpringApiApplication.class, args);
		SpringApplication app = new SpringApplication(NewSpringApiApplication.class);
		app.setDefaultProperties(Collections.singletonMap("server.port", "8083"));
		app.run(args);
	}

}
