package com.xsis.siakad.repository;

import org.springframework.data.jpa.repository.JpaRepository;

import com.xsis.siakad.model.Jurusan;

public interface JurusanRepo extends JpaRepository<Jurusan, Integer> {

}
