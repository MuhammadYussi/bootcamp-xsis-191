package com.xsis.siakad.model;

import java.util.ArrayList;
import java.util.List;

import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.ForeignKey;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.OneToMany;
import javax.persistence.Table;
import javax.persistence.TableGenerator;

import com.fasterxml.jackson.annotation.JsonBackReference;
import com.fasterxml.jackson.annotation.JsonManagedReference;

@Entity
@Table(name="jurusan")
public class Jurusan {
	
	@Id
	@GeneratedValue(strategy=GenerationType.TABLE, generator="jurusan_seq")
	@TableGenerator(name="jurusan_seq", table="tbl_sequence", pkColumnName="seq_id", valueColumnName="seq_value", initialValue=0, allocationSize=1)
	@Column(name = "id")
	private int id;
	
	@Column(name="kd_jurusan", nullable=false, length=10)
	private String kode;
	
	@Column(name="nm_jurusan", nullable=false, length=150)
	private String nama;
	
	@Column(name="fakultas_id", nullable=false, updatable=false, insertable=false)
	private int fakultasId;
	
	@JsonBackReference
	@ManyToOne
	@JoinColumn(name="fakultas_id", foreignKey=@ForeignKey(name="fk_fakultas"), updatable=false, insertable=false)
	private Fakultas fakultas;
	
	@JsonManagedReference
	@OneToMany(mappedBy="jurusan", cascade=CascadeType.ALL)
	private List<Matkul> listMatkul = new ArrayList<Matkul>();
	
	@JsonManagedReference
	@OneToMany(mappedBy="jurusan", cascade=CascadeType.ALL)
	private List<Mahasiswa> listMahasiswa = new ArrayList<Mahasiswa>();

	public int getId() {
		return id;
	}

	public void setId(int id) {
		this.id = id;
	}

	public String getKode() {
		return kode;
	}

	public void setKode(String kode) {
		this.kode = kode;
	}

	public String getNama() {
		return nama;
	}

	public void setNama(String nama) {
		this.nama = nama;
	}

	public int getFakultasId() {
		return fakultasId;
	}

	public void setFakultasId(int fakultasId) {
		this.fakultasId = fakultasId;
	}

	public Fakultas getFakultas() {
		return fakultas;
	}

	public void setFakultas(Fakultas fakultas) {
		this.fakultas = fakultas;
	}

	public List<Matkul> getListMatkul() {
		return listMatkul;
	}

	public void setListMatkul(List<Matkul> listMatkul) {
		this.listMatkul = listMatkul;
	}

	public List<Mahasiswa> getListMahasiswa() {
		return listMahasiswa;
	}

	public void setListMahasiswa(List<Mahasiswa> listMahasiswa) {
		this.listMahasiswa = listMahasiswa;
	}
}
