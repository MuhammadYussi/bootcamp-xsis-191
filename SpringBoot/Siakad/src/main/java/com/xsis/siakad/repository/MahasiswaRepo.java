package com.xsis.siakad.repository;

import org.springframework.data.jpa.repository.JpaRepository;

import com.xsis.siakad.model.Mahasiswa;

public interface MahasiswaRepo extends JpaRepository<Mahasiswa, Integer>{

}
