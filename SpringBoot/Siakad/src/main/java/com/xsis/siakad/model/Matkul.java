package com.xsis.siakad.model;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.ForeignKey;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.Table;
import javax.persistence.TableGenerator;

import com.fasterxml.jackson.annotation.JsonBackReference;

@Entity
@Table(name="matkul")
public class Matkul {
	
	@Id
	@GeneratedValue(strategy=GenerationType.TABLE, generator="matkul_seq")
	@TableGenerator(name="fakultas_seq", table="tbl_sequence",pkColumnName="seq_id",valueColumnName="seq_value",initialValue=0,allocationSize=1)
	@Column(name="id", nullable=false)
	private int id;
	
	@Column(name="kd_matkul", nullable=false, length=10)
	private String kode;
	
	@Column(name="nm_matkul", nullable=false, length=150)
	private String nama;
	
	@Column(name="sks", nullable=false, length=10)
	private int sks;
	
	@Column(name="jurusan_id", nullable=false)
	private int jurusanId;
	
	@JsonBackReference
	@ManyToOne
	@JoinColumn(name="jurusan_id", foreignKey=@ForeignKey(name="fk_jurusan_matkul"),updatable=false, insertable=false)
	private int jurusan;

	public int getId() {
		return id;
	}

	public void setId(int id) {
		this.id = id;
	}

	public String getKode() {
		return kode;
	}

	public void setKode(String kode) {
		this.kode = kode;
	}

	public String getNama() {
		return nama;
	}

	public void setNama(String nama) {
		this.nama = nama;
	}

	public int getSks() {
		return sks;
	}

	public void setSks(int sks) {
		this.sks = sks;
	}

	public int getJurusanId() {
		return jurusanId;
	}

	public void setJurusanId(int jurusanId) {
		this.jurusanId = jurusanId;
	}

	public int getJurusan() {
		return jurusan;
	}

	public void setJurusan(int jurusan) {
		this.jurusan = jurusan;
	}
}
