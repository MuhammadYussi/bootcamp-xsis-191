package com.xsis.siakad.repository;

import org.springframework.data.jpa.repository.JpaRepository;

import com.xsis.siakad.model.Fakultas;

public interface FakultasRepo extends JpaRepository<Fakultas, Integer> {

}
