package com.xsis.siakad.model;

import java.util.ArrayList;
import java.util.List;

import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.OneToMany;
import javax.persistence.Table;
import javax.persistence.TableGenerator;

import com.fasterxml.jackson.annotation.JsonManagedReference;

@Entity
@Table(name="fakultas")
public class Fakultas {
	
	@Id
	@GeneratedValue(strategy=GenerationType.TABLE,generator="fakultas_seq")
	@TableGenerator(name="fakulats_seq", table="tbl_sequence", pkColumnName="seq_id", valueColumnName="seq_value", initialValue=0, allocationSize=1)
	@Column(name="id", nullable=false)
	private Integer id;
	
	@Column(name="kd_fakultas", nullable=false, length=10)
	private String kode;
	
	@Column(name="nm_fakultas", nullable=false, length=150)
	private String nama;
	
	@JsonManagedReference
	@OneToMany(mappedBy="fakultas", cascade=CascadeType.ALL)
	private List<Jurusan> listJurusan = new ArrayList<Jurusan>();

	public Integer getId() {
		return id;
	}

	public void setId(Integer id) {
		this.id = id;
	}

	public String getKode() {
		return kode;
	}

	public void setKode(String kode) {
		this.kode = kode;
	}

	public String getNama() {
		return nama;
	}

	public void setNama(String nama) {
		this.nama = nama;
	}

	public List<Jurusan> getListJurusan() {
		return listJurusan;
	}

	public void setListJurusan(List<Jurusan> listJurusan) {
		this.listJurusan = listJurusan;
	}
}
