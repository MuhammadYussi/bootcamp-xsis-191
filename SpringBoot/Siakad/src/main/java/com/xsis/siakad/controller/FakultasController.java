package com.xsis.siakad.controller;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;

import com.xsis.siakad.model.Fakultas;
import com.xsis.siakad.repository.FakultasRepo;

@Controller
public class FakultasController {
	
	@Autowired
	private FakultasRepo repo;
	
	@RequestMapping(value="/fakultas", method=RequestMethod.GET)
	public String index(Model model) {
		List<Fakultas> data = repo.findAll();
		model.addAttribute("listdata",data);
		return "fakultas/index";
	}
	
	@RequestMapping(value="/fakultas/add")
	public String add() {
		return "fakultas/add";
	}
	
	@RequestMapping(value="/fakultas/save", method=RequestMethod.POST)
	public String save(@ModelAttribute Fakultas item) {
		repo.save(item);
		return "redirect:/fakultas/index";
	}
}
