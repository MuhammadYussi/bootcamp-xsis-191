package com.xsis.siakad.repository;

import org.springframework.data.jpa.repository.JpaRepository;

import com.xsis.siakad.model.Matkul;

public interface MatkulRepo extends JpaRepository<Matkul, Integer> {

}
