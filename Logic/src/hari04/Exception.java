package hari04;

public class Exception {
	public static void main(String[] args) {
		try {
			int[] a = new int[2];
			System.out.println("Access Element Three : "+a[3]);
		}catch (ArrayIndexOutOfBoundsException e) {
			System.out.println("Exception Thrown : "+e);
		}
		System.out.println("Out Of the Block");
	}
}
