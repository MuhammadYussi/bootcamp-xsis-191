package hari04;

public class ExceptionFinal {
	public static void main(String[] args) {
		int[] a = new int[2];
		try {
			System.out.println("Access Element Three : "+a[3]);
		}catch (ArrayIndexOutOfBoundsException e) {
			System.out.println("Exception Thrown : "+e);
		}finally {
			a[0] = 6;
			System.out.println("First Element Value : "+a[0]);
			System.out.println("The Finally Statment is Executed");
		}
	}
}
