package hari07;

public class AbstractTest01 {
	public static void main(String[] args) {
		Shape01 bentuk1 = new Circle01();
		bentuk1.draw();
		Shape01 bentuk2 = new Rectangle01();
		bentuk2.draw();
		Shape01 bentuk3 = new Triangle01();
		bentuk3.draw();
		Shape01 bentuk4 = new Square01();
		bentuk4.draw();
		Shape01 bentuk5 = new Oval01();
		bentuk5.draw();
	}
}
