package hari07;

public class AbstractClass01 {
	public static void main(String[] args) {
		Bike01 obj1 = new Honda01();
		Bike01 obj2 = new Yamaha01();
		Bike01 obj3 = new Suzuki();
		obj1.run();
		obj1.speed();
		obj2.run();
		obj3.run();
	}
}
