package hackerRank.Implementation;
import java.util.Scanner;

public class SolveMeFirst2 {
    static int solveMeFirst(int c, int d) {
        return c+d;
   }

    public static void main(String[] args) {
    	Scanner input2 = new Scanner(System.in); 
    	System.out.print("a : ");
    	int c = input2.nextInt();
    	System.out.print("b : ");
        int d = input2.nextInt();
        int sum = solveMeFirst(c, d);
        System.out.println("Output : "+c+" + "+d+" = "+sum);
        input2.close();
   }
}
