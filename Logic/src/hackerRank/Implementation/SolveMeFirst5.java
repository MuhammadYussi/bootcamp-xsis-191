package hackerRank.Implementation;
import java.util.Scanner;

public class SolveMeFirst5 {
    static int solveMeFirst(int i, int j) {
        return i+j;
   }

    public static void main(String[] args) {
    	Scanner input5 = new Scanner(System.in); 
    	System.out.print("a : ");
    	int i = input5.nextInt();
    	System.out.print("b : ");
        int j = input5.nextInt();
        int sum = solveMeFirst(i, j);
        System.out.println("Output : "+i+" + "+j+" = "+sum);
        input5.close();
   }
}
