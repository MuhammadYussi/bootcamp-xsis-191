package hackerRank.Implementation;

public class GradingStudent1 {
	static  int[] gradingStudent1(int[] grade) {
		for (int i=0; i<grade.length; i++) {				//perulangan menghitung nilai grade
			if(grade[i]>=38) {								//jika grade index ke-i lebih dari 38
				if((5-grade[i]%5)<3) {						//jika hasil mod kurang dari 3
					grade[i]=grade[i]+(5-grade[i]%5);		//tambah grade index ke-i dari sisa grades
				}
			}
		}
		return grade;										//mengembalikan nilai grade
	}
	
	public static void main(String[] args) {
		int [] a = {73,67,38,33};							//nilai
		int [] b = gradingStudent1(a);						//variable b akan berisi perhitungan gradingStudent1
		for (int i = 0; i < a.length; i++) {				//perulangan menampilkan hasil grade
			System.out.println(b[i]);						//output
		}
	}
}