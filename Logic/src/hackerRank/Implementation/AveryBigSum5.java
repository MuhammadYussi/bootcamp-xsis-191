package hackerRank.Implementation;

public class AveryBigSum5 {
	static long aVeryBigSum(long[] ar5) {
		long e = 0;
		for(int m=0; m<ar5.length; m++) {
			e+=ar5[m];
		}
		return e;
	}
	
	public static void main(String[] args) {
		long[] ar5 = {134553,134128,893867,622949,99897};
		long e=aVeryBigSum(ar5);
		System.out.println(e);
	}
}
