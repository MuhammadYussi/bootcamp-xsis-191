package hackerRank.Implementation;

public class Staircase1 {
	static int stair1(int n) {
		for(int i=0;i<n;i++) {
			for(int j=0;j<n;j++) {
				if(j<n-1-i) {				//jika nilai j kurang dari nilai n-1-i
					System.out.print(" ");	//maka akan menampilkan spasi
				}else {						//jika tidak
					System.out.print("#");	//maka baru menampilkan "#"
				}
			}System.out.println(" ");		//membuat line baru
		}
		return n;							//mengembalikan nilai n
	}
	public static void main(String[] args) {
		int n = 5;							// nilai n adalah 5
		stair1(n);							//menampilkan hasil perhitungan stair1 dengan nilai n
	}
}
