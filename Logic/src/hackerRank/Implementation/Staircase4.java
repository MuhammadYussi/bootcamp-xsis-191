package hackerRank.Implementation;

public class Staircase4 {
	static int stair4(int q) {
		for(int l=0;l<q;l++) {
			for(int m=0;m<q;m++) {
				if(m<q-1-l) {
					System.out.print(" ");
				}else {
					System.out.print("#");
				}
			}System.out.println(" ");
		}
		return q;
	}
	public static void main(String[] args) {
		int q = 8;
		stair4(q);
	}
}
