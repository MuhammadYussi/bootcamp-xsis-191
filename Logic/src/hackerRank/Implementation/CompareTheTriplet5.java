package hackerRank.Implementation;
import java.util.*;

public class CompareTheTriplet5 {
	static List<Integer> compareTriplets(List<Integer> a5, List<Integer> b5) {

        List<Integer> result5 = new ArrayList<Integer>();
        result5.add(0);
        result5.add(0);
        int n4Alice = 0;
        int n4Bob = 0;
        for(int m=0; m<a5.size(); m++){
            if(a5.get(m)>b5.get(m)){
                n4Alice++;
                result5.set(0,n4Alice);
            }
            if(a5.get(m)<b5.get(m)){
                n4Bob++;
                result5.set(1,n4Bob);
            }
        }
        return result5;
    }
	public static void main(String[] args) {
		List<Integer> a5 = new ArrayList<Integer>();
        a5.add(82);
        a5.add(91);
        a5.add(2);
        
        
        List<Integer> b5 = new ArrayList<Integer>();
        b5.add(99);
        b5.add(66);
        b5.add(42);
        
        for (Integer item: compareTriplets(a5, b5)) {
        	System.out.println(item + "\t");
        }
	}
}
