package hackerRank.Implementation;
import java.util.*;

public class CompareTheTriplet2 {
	static List<Integer> compareTriplets(List<Integer> a2, List<Integer> b2) {

        List<Integer> result2 = new ArrayList<Integer>();
        result2.add(0);
        result2.add(0);
        int n2Alice = 0;
        int n2Bob = 0;
        for(int j=0; j<a2.size(); j++){
            if(a2.get(j)>b2.get(j)){
                n2Alice++;
                result2.set(0,n2Alice);
            }
            if(a2.get(j)<b2.get(j)){
                n2Bob++;
                result2.set(1,n2Bob);
            }
        }
        return result2;
    }
	public static void main(String[] args) {
		List<Integer> a2 = new ArrayList<Integer>();
        a2.add(45);
        a2.add(60);
        a2.add(10);
        
        
        List<Integer> b2 = new ArrayList<Integer>();
        b2.add(69);
        b2.add(66);
        b2.add(42);
        
        for (Integer item: compareTriplets(a2, b2)) {
        	System.out.println(item + "\t");
        }
	}
}
