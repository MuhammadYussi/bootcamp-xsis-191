package hackerRank.Implementation;

public class AveryBigSum4 {
	static long aVeryBigSum(long[] ar4) {
		long d = 0;
		for(int l=0; l<ar4.length; l++) {
			d+=ar4[l];
		}
		return d;
	}
	
	public static void main(String[] args) {
		long[] ar4 = {34553,129128,83867,2949,59897};
		long d=aVeryBigSum(ar4);
		System.out.println(d);
	}
}
