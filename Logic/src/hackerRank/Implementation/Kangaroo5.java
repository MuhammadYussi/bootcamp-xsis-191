package hackerRank.Implementation;
import java.util.Scanner;

public class Kangaroo5 {
	static String kangaroo(int g1, int h1, int g2, int h2) {
		String c = "YES";
        if(g1<h1 && g2<h2){
           c =  "NO";
        }else{
            if(g2!=h2 && (h1-g1)%(g2-h2)==0){
                c = "YES";
            }else{
                c="NO";
            }
        }
        return c;
    }

	public static void main(String[] args) {
		// TODO Auto-generated method stub
		Scanner input4 = new Scanner(System.in);
		System.out.println("x1 : ");
		int g1 = input4.nextInt();
		System.out.println("x2 : ");
		int h1 = input4.nextInt();
		System.out.println("v1 : ");
		int g2 = input4.nextInt();
		System.out.println("v2 : ");
		int h2 = input4.nextInt();
		
		String n=kangaroo(g1, h1, g2, h2);
		System.out.println("Result : "+n);
		input4.close();
	}
}
