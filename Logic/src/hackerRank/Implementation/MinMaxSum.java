package hackerRank.Implementation;
import java.util.Arrays;

public class MinMaxSum {
	static void miniMaxSum(int[] arr) {
        long max1 = 0;
        long min1 = 0;
        
        Arrays.sort(arr);
        for(int j = 0; j < arr.length-1; j++){
            min1 = min1 + arr[j];
         }

        for(int i = 1; i < arr.length; i++){
            max1 = max1 + arr[i];
        }
        System.out.print(min1 + " "+ max1);

    }
}
