package hackerRank.Implementation;

public class AveryBigSum3 {
	static long aVeryBigSum(long[] ar3) {
		long c = 0;
		for(int k=0; k<ar3.length; k++) {
			c+=ar3[k];
		}
		return c;
	}
	
	public static void main(String[] args) {
		long[] ar3 = {234553,29128,3867,72949,509897};
		long c=aVeryBigSum(ar3);
		System.out.println(c);
	}
}
