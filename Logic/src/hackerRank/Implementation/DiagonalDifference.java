package hackerRank.Implementation;

public class DiagonalDifference {
	static int diagonalDifference(int[][] arr) {
        int a = 0;
        int b = 0;

        for(int i=0;i<arr.length;i++){
            a+= arr[i][i];
            b+= arr[i][arr.length-1-i];
        }

        if(b>a){
            return b-a;
        }else{
            return a-b;
        }
	}
}
