package hackerRank.Implementation;

public class AveryBigSum2 {
	static long aVeryBigSum(long[] ar2) {
		long b = 0;
		for(int j=0; j<ar2.length; j++) {
			b+=ar2[j];
		}
		return b;
	}
	
	public static void main(String[] args) {
		long[] ar2 = {2345,291238,381267,72349,5097};
		long b=aVeryBigSum(ar2);
		System.out.println(b);
	}
}
