package hackerRank.Implementation;

public class GradingStudent3 {
	static  int[] gradingStudent3(int[] grade3) {
		for (int k=0; k<grade3.length; k++) {
			if(grade3[k]>=38) {
				if((5-grade3[k]%5)<3) {
					grade3[k]=grade3[k]+(5-grade3[k]%5);
				}
			}
		}
		return grade3;
	}
	
	public static void main(String[] args) {
		int [] e = {37,76,83,66};
		int [] f = gradingStudent3(e);
		for (int k = 0; k < e.length; k++) {				
			System.out.println(f[k]);						
		}
	}
}
