package hackerRank.Implementation;
import java.util.Scanner;

public class SolveMeFirst4 {
    static int solveMeFirst(int g, int h) {
        return g+h;
   }

    public static void main(String[] args) {
    	Scanner input4 = new Scanner(System.in); 
    	System.out.print("a : ");
    	int g = input4.nextInt();
    	System.out.print("b : ");
        int h = input4.nextInt();
        int sum = solveMeFirst(g, h);
        System.out.println("Output : "+g+" + "+h+" = "+sum);
        input4.close();
   }
}
