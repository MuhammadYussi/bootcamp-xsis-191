package hackerRank.Implementation;
import java.util.Scanner;

public class Kangaroo1 {
	static String kangaroo(int x1, int v1, int x2, int v2) {	//Kangaroo mempunyai 4 parameter
		String a = "YES";						//kondisi awal a
        if(x1<x2 && v1<v2){
           a =  "NO";							//output jika x1<x2 dan v1<v2
        }else{									//jika kondisi tidak terpenuhi
            if(v1!=v2 && (x2-x1)%(v1-v2)==0){
                a = "YES";						//output jika v1 tidak sama dengan v2 dan x2-x1 modulus v1-v2 adalah 0
            }else{
                a="NO";							//output jika tidak terpenuhi
            }
        }
        return a;								//mengembalikan nilai
    }

	public static void main(String[] args) {
		// TODO Auto-generated method stub
		Scanner input = new Scanner(System.in);
		System.out.println("x1 : ");
		int x1 = input.nextInt();
		System.out.println("x2 : ");
		int x2 = input.nextInt();
		System.out.println("v1 : ");
		int v1 = input.nextInt();
		System.out.println("v2 : ");
		int v2 = input.nextInt();
		
		String hasil=kangaroo(x1, v1, x2, v2);
		System.out.println("Result : "+hasil);
		input.close();
	}
}
