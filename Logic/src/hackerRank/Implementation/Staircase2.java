package hackerRank.Implementation;

public class Staircase2 {
	static int stair2(int o) {
		for(int j=0;j<o;j++) {
			for(int k=0;k<o;k++) {
				if(k<o-1-j) {
					System.out.print(" ");
				}else {
					System.out.print("#");
				}
			}System.out.println(" ");
		}
		return o;
	}
	public static void main(String[] args) {
		int o = 6;
		stair2(o);
	}
}
