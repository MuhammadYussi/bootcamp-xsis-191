package hackerRank.Implementation;
import java.util.Scanner;

public class SolveMeFirst3 {
    static int solveMeFirst(int e, int f) {
        return e+f;
   }

    public static void main(String[] args) {
    	Scanner input3 = new Scanner(System.in); 
    	System.out.print("a : ");
    	int e = input3.nextInt();
    	System.out.print("b : ");
        int f = input3.nextInt();
        int sum = solveMeFirst(e, f);
        System.out.println("Output : "+e+" + "+f+" = "+sum);
        input3.close();
   }
}
