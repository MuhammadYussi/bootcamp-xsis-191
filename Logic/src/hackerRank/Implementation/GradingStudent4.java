package hackerRank.Implementation;

public class GradingStudent4 {
	static  int[] gradingStudent4(int[] grade4) {
		for (int l=0; l<grade4.length; l++) {
			if(grade4[l]>=38) {
				if((5-grade4[l]%5)<3) {
					grade4[l]=grade4[l]+(5-grade4[l]%5);
				}
			}
		}
		return grade4;
	}
	
	public static void main(String[] args) {
		int [] g = {67,38,33,73};
		int [] h = gradingStudent4(g);
		for (int l = 0; l < g.length; l++) {				
			System.out.println(h[l]);						
		}
	}
}
