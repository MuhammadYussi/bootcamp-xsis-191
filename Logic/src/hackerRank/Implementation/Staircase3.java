package hackerRank.Implementation;

public class Staircase3 {
	static int stair3(int p) {
		for(int k=0;k<p;k++) {
			for(int l=0;l<p;l++) {
				if(l<p-1-k) {
					System.out.print(" ");
				}else {
					System.out.print("#");
				}
			}System.out.println(" ");
		}
		return p;
	}
	public static void main(String[] args) {
		int p = 7;
		stair3(p);
	}
}
