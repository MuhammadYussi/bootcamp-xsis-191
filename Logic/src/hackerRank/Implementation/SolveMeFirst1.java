package hackerRank.Implementation;
import java.util.Scanner;

public class SolveMeFirst1 {
    static int solveMeFirst(int a, int b) {		//membuat method solveMeFirst dengan parameter int a dan int b
        return a+b;								//mengembalikan nilai a+b ke method solveMeFirst
   }

    public static void main(String[] args) {
    	Scanner input1 = new Scanner(System.in);	//membuat scanner input 
    	System.out.print("a : ");
    	int a = input1.nextInt();				//nilai a bertipe int
    	System.out.print("b : ");
        int b = input1.nextInt();				//nilai b bertipe int
        int sum = solveMeFirst(a, b);			//variable sum akan menghasilkan perhitungan dari method solveMeFirst
        System.out.println("Output : "+a+" + "+b+" = "+sum); //output
        input1.close();
   }
}
