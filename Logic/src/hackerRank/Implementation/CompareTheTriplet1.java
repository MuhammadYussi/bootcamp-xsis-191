package hackerRank.Implementation;
import java.util.*;

public class CompareTheTriplet1 {
	static List<Integer> compareTriplets(List<Integer> a, List<Integer> b) {
        List<Integer> result = new ArrayList<Integer>();	
        result.add(0);
        result.add(0);
        int nAlice = 0;
        int nBob = 0;
        for(int i=0; i<a.size(); i++){
            if(a.get(i)>b.get(i)){				
                nAlice++;
                result.set(0,nAlice);
            }
            if(a.get(i)<b.get(i)){
                nBob++;
                result.set(1,nBob);
            }
        }
        return result;
    }
	
	public static void main(String[] args) {
		List<Integer> a = new ArrayList<Integer>();
        a.add(17);
        a.add(28);
        a.add(30);
        
        
        List<Integer> b = new ArrayList<Integer>();
        b.add(99);
        b.add(16);
        b.add(12);
        
        for (Integer item: compareTriplets(a, b)) {
        	System.out.println(item + "\t");
        }
	}
}
