package hackerRank.Implementation;
import java.util.Scanner;

public class Kangaroo3 {
	static String kangaroo(int c1, int d1, int c2, int d2) {
		String c = "YES";
        if(c1<d1 && c2<d2){
           c =  "NO";
        }else{
            if(c2!=d2 && (d1-c1)%(c2-d2)==0){
                c = "YES";
            }else{
                c="NO";
            }
        }
        return c;
    }

	public static void main(String[] args) {
		// TODO Auto-generated method stub
		Scanner input2 = new Scanner(System.in);
		System.out.println("x1 : ");
		int c1 = input2.nextInt();
		System.out.println("x2 : ");
		int d1 = input2.nextInt();
		System.out.println("v1 : ");
		int c2 = input2.nextInt();
		System.out.println("v2 : ");
		int d2 = input2.nextInt();
		
		String result=kangaroo(c1, d1, c2, d2);
		System.out.println("Result : "+result);
		input2.close();
	}
}
