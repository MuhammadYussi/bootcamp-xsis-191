package hackerRank.Implementation;
import java.util.*;

public class CompareTheTriplet4 {
	static List<Integer> compareTriplets(List<Integer> a4, List<Integer> b4) {

        List<Integer> result4 = new ArrayList<Integer>();
        result4.add(0);
        result4.add(0);
        int n4Alice = 0;
        int n4Bob = 0;
        for(int l=0; l<a4.size(); l++){
            if(a4.get(l)>b4.get(l)){
                n4Alice++;
                result4.set(0,n4Alice);
            }
            if(a4.get(l)<b4.get(l)){
                n4Bob++;
                result4.set(1,n4Bob);
            }
        }
        return result4;
    }
	public static void main(String[] args) {
		List<Integer> a4 = new ArrayList<Integer>();
        a4.add(12);
        a4.add(54);
        a4.add(71);
        
        
        List<Integer> b4 = new ArrayList<Integer>();
        b4.add(10);
        b4.add(24);
        b4.add(4);
        
        for (Integer item: compareTriplets(a4, b4)) {
        	System.out.println(item + "\t");
        }
	}
}
