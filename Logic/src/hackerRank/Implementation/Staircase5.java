package hackerRank.Implementation;

public class Staircase5 {
	static int stair5(int r) {
		for(int m=0;m<r;m++) {
			for(int n=0;n<r;n++) {
				if(n<r-1-m) {
					System.out.print(" ");
				}else {
					System.out.print("#");
				}
			}System.out.println(" ");
		}
		return r;
	}
	public static void main(String[] args) {
		int r = 9;
		stair5(r);
	}
}
