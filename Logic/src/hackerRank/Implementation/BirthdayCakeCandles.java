package hackerRank.Implementation;

public class BirthdayCakeCandles {
	static int birthdayCakeCandles(int[] ar) {
        int max=ar[0];
        int num=1;
        for (int i=1;i<ar.length;i++){
            if (ar[i]>max){ 
            max=ar[i];
            num=1;
            }
            else if (ar[i]==max) num++;
        }
        return num;
    }
}
