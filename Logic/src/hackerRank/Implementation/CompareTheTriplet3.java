package hackerRank.Implementation;
import java.util.*;

public class CompareTheTriplet3 {
	static List<Integer> compareTriplets(List<Integer> a3, List<Integer> b3) {

        List<Integer> result3 = new ArrayList<Integer>();
        result3.add(0);
        result3.add(0);
        int n3Alice = 0;
        int n3Bob = 0;
        for(int k=0; k<a3.size(); k++){
            if(a3.get(k)>b3.get(k)){
                n3Alice++;
                result3.set(0,n3Alice);
            }
            if(a3.get(k)<b3.get(k)){
                n3Bob++;
                result3.set(1,n3Bob);
            }
        }
        return result3;
    }
	public static void main(String[] args) {
		List<Integer> a3 = new ArrayList<Integer>();
        a3.add(80);
        a3.add(32);
        a3.add(77);
        
        
        List<Integer> b3 = new ArrayList<Integer>();
        b3.add(10);
        b3.add(61);
        b3.add(12);
        
        for (Integer item: compareTriplets(a3, b3)) {
        	System.out.println(item + "\t");
        }
	}
}
