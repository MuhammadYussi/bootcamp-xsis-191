package hackerRank.Implementation;

public class AppleAndOrange {
	 static void countApplesAndOranges(int s, int t, int a, int b, int[] apples, int[] oranges) {
	        int apel = 0;
	        int orange = 0;
	        for(int i=0; i<apples.length; i++){
	            int jarakA = a+apples[i];
	            if(jarakA >= s && jarakA <= t){
	                apel++;
	            }
	        }
	        for(int j=0; j<oranges.length; j++){
	            int jarakB = b+oranges[j];
	            if(jarakB >= s && jarakB <=t){
	                orange++;
	            }
	        }
	        System.out.println(apel);
	        System.out.println(orange);
	    }
}
