package hackerRank.Implementation;

public class AveryBigSum1 {
	static long aVeryBigSum(long[] ar1) {		//ar1 bertipe long
		long a = 0;								//nilai a = 0
		for(int i=0; i<ar1.length; i++) {		//perulangan untuk menjumlahkan
			a+=ar1[i];							//nilai a akan ditambah ar1 index ke-i
		}
		return a;								//mengembalikan nilai
	}
	
	public static void main(String[] args) {
		long[] ar1 = {138892,291238,387126,47239,509237};	//nilai array ar1
		long a=aVeryBigSum(ar1);							//nilai a berisi hasil perhitungan ar1
		System.out.println(a);								//output
	}
}
