package hackerRank.Implementation;

public class GradingStudent5 {
	static  int[] gradingStudent5(int[] grade5) {
		for (int m=0; m<grade5.length; m++) {
			if(grade5[m]>=38) {
				if((5-grade5[m]%5)<3) {
					grade5[m]=grade5[m]+(5-grade5[m]%5);
				}
			}
		}
		return grade5;
	}
	
	public static void main(String[] args) {
		int [] i = {54,74,88,56};
		int [] j = gradingStudent5(i);
		for (int m = 0; m < i.length; m++) {				
			System.out.println(j[m]);						
		}
	}
}
