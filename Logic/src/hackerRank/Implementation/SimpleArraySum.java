package hackerRank.Implementation;

public class SimpleArraySum {
	static int simpleArraySum(int[] ar) {
        int sum = 0;
        for(int count = 0; count < ar.length; count++) {
        	sum = sum + ar[count];
    	}
    	return sum;
	}
}
