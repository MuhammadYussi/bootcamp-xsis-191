package hackerRank.Implementation;

public class GradingStudent2 {
	static  int[] gradingStudent2(int[] grade2) {
		for (int j=0; j<grade2.length; j++) {
			if(grade2[j]>=38) {
				if((5-grade2[j]%5)<3) {
					grade2[j]=grade2[j]+(5-grade2[j]%5);
				}
			}
		}
		return grade2;
	}
	
	public static void main(String[] args) {
		int [] c = {63,37,88,23};
		int [] d = gradingStudent2(c);
		for (int j = 0; j < c.length; j++) {				
			System.out.println(d[j]);						
		}
	}
}