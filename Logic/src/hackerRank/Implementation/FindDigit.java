package hackerRank.Implementation;

public class FindDigit {
	static int findDigits(int n) {
		int tempN = n;
		int digit = 0;
		int output = 0;

		while(tempN > 0) {
			digit = tempN%10;
			tempN = tempN/10;
			if(digit > 0 && n % digit == 0) {
				output++;
			}
		}
		
		System.out.println(output);
		return output;
	}
}
