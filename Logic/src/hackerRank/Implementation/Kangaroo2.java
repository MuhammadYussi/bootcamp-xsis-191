package hackerRank.Implementation;
import java.util.Scanner;

public class Kangaroo2 {
	static String kangaroo(int a1, int b1, int a2, int b2) {
		String b = "YES";
        if(a1<b1 && a2<b2){
           b =  "NO";
        }else{
            if(a2!=b2 && (b1-a1)%(a2-b2)==0){
                b = "YES";
            }else{
                b="NO";
            }
        }
        return b;
    }

	public static void main(String[] args) {
		// TODO Auto-generated method stub
		Scanner input1 = new Scanner(System.in);
		System.out.println("x1 : ");
		int a1 = input1.nextInt();
		System.out.println("x2 : ");
		int a2 = input1.nextInt();
		System.out.println("v1 : ");
		int b1 = input1.nextInt();
		System.out.println("v2 : ");
		int b2 = input1.nextInt();
		
		String output=kangaroo(a1, b1, a2, b2);
		System.out.println("Result : "+output);
		input1.close();
	}
}
