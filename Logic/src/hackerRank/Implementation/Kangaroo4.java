package hackerRank.Implementation;
import java.util.Scanner;

public class Kangaroo4 {
	static String kangaroo(int e1, int f1, int e2, int f2) {
		String c = "YES";
        if(e1<f1 && e2<f2){
           c =  "NO";
        }else{
            if(e2!=f2 && (f1-e1)%(e2-f2)==0){
                c = "YES";
            }else{
                c="NO";
            }
        }
        return c;
    }

	public static void main(String[] args) {
		// TODO Auto-generated method stub
		Scanner input3 = new Scanner(System.in);
		System.out.println("x1 : ");
		int e1 = input3.nextInt();
		System.out.println("x2 : ");
		int f1 = input3.nextInt();
		System.out.println("v1 : ");
		int e2 = input3.nextInt();
		System.out.println("v2 : ");
		int f2 = input3.nextInt();
		
		String nilai=kangaroo(e1, f1, e2, f2);
		System.out.println("Result : "+nilai);
		input3.close();
	}
}
