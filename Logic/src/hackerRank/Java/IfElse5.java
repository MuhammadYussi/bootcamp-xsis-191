package hackerRank.Java;
import java.util.Scanner;

public class IfElse5 {
	static Scanner input5;

    public static void main(String[] args) {
    	input5 = new Scanner(System.in);
        int e = input5.nextInt();
        if(e%2==1){
            System.out.println("Weird");
        }else{
            if(e>=2 && e<5){
                System.out.println("Not Weird");
            }else if(e>=6 && e<=20){
                System.out.println("Weird");
            }else if(e>20){
                System.out.println("Not Weird");
            }
        }

        input5.close();
    }
}
