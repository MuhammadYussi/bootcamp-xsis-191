package hackerRank.Java;
import java.util.Scanner;

public class Loops2_2 {
	static Scanner input2;
	public static void main(String []argh){
        Scanner input1 = new Scanner(System.in);
        System.out.print("Input : ");
        int q2=input1.nextInt();
        for(int k=0;k<q2;k++){
        	System.out.print("Nilai Awal : ");
            int a2 = input1.nextInt();
            System.out.print("Nilai Pengkali : ");
            int b2 = input1.nextInt();
            System.out.print("Total Perhitungan : ");
            int n2 = input1.nextInt();

            for(int l=0; l<n2; l++){
                a2 = a2+b2;
                System.out.print(a2+" ");
                b2 = b2*2;
            }
            System.out.println();
        }
        input1.close();
    }
}
