package hackerRank.Java;
import java.util.Scanner;

public class IfElse1 {
	static Scanner input1;

    public static void main(String[] args) {
    	input1 = new Scanner(System.in);				//input sebagai scanner baru
        int a = input1.nextInt();					//int a berupa inputan user
        if(a%2==1){									//kondisi ketika bilangan bukan genap
            System.out.println("Weird");			//maka akan print "Weird"
        }else{										//kondisi sebaliknya(jika bilangan genap)
            if(a>=2 && a<5){						//jika a lebih dari samadengan 2 dan kurang dari 5  
                System.out.println("Not Weird");	//print "Not Weird"
            }else if(a>=6 && a<=20){				//jika a lebih dari samadengan 6 dan kurang dari samadengan 20
                System.out.println("Weird");		//print "Weird"
            }else if(a>20){							//jika a lebih dari 20
                System.out.println("Not Weird");	//print "Not Weird"
            }
        }

        input1.close();								//menutup scanner untuk tidak menerima inputan
    }
}
