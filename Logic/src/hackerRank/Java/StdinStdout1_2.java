package hackerRank.Java;
import java.util.Scanner;

public class StdinStdout1_2 {
	static Scanner scan2;
	public static void main(String[] args) {
        Scanner scan2 = new Scanner(System.in);
        System.out.print("Input 1 : ");
        int a2 = scan2.nextInt();
        System.out.print("Input 2 : ");
        int b2 = scan2.nextInt();
        System.out.print("Input 3 : ");
        int c2 = scan2.nextInt();

        System.out.println("");
        System.out.println(a2);
        System.out.println(b2);
        System.out.println(c2);
        scan2.close();
	}
}