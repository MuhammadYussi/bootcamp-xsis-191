package hackerRank.Java;
import java.util.Scanner;

public class StdinStdout2_1 {
	static Scanner input1;
	public static void main(String[] args) {
		Scanner input1 = new Scanner(System.in);	//membuat field scanner baru
		System.out.print("INT : ");
        int a1 = input1.nextInt();				//memberikan nilai a sebagai masukkan user bertipe int
        System.out.print("Double : ");
        Double b1 = input1.nextDouble();			//memberikan nilai b sebagai masukkan user bertipe Double
        System.out.print("String : ");
        String c1 = input1.nextLine();			//memberikan nilai c sebagai masukkan user bertipe String

        System.out.println("String: " + c1);		//Output berupa String
        System.out.println("Double: " + b1);		//Output berupa Double
        System.out.println("Int: " + a1);		//Output berupa Integer
        
        input1.close();
	}
}
