package hackerRank.Java;
import java.util.Scanner;

public class Java1DArray4 {
	static Scanner scan4;
	public static void main(String[] args) {
		Scanner scan4 = new Scanner(System.in);
		System.out.print("Total : ");
        int a4 = scan4.nextInt();
        int[] b4 = new int[a4];
        for(int l=0; l<a4; l++){
        	System.out.print("Nilai "+l+" : ");
        	b4[l] = scan4.nextInt();
        }
        scan4.close();

        for (int l=0; l<b4.length; l++) {
            System.out.print(b4[l]+" ");
        }
	}
}
