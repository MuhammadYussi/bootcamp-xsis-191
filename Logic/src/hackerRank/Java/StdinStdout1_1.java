package hackerRank.Java;
import java.util.Scanner;

public class StdinStdout1_1 {
	static Scanner scan1;
	public static void main(String[] args) {
        Scanner scan1 = new Scanner(System.in);
        System.out.print("Input 1 : ");
        int a1 = scan1.nextInt();			//input nilai a
        System.out.print("Input 2 : ");
        int b1 = scan1.nextInt();			//input nilai b
        System.out.print("Input 3 : ");
        int c1 = scan1.nextInt();			//input nilai c

        System.out.println("");
        System.out.println(a1);			//output nilai a
        System.out.println(b1);			//output nilai b
        System.out.println(c1);			//output nilai c
        scan1.close();
	}
}
