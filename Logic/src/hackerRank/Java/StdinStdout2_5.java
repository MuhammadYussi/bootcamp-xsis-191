package hackerRank.Java;
import java.util.Scanner;

public class StdinStdout2_5 {
	static Scanner input5;
	public static void main(String[] args) {
		Scanner input5 = new Scanner(System.in);
		System.out.print("INT : ");
        int a5 = input5.nextInt();
        System.out.print("Double : ");
        Double b5 = input5.nextDouble();
        System.out.print("String : ");
        String c5 = input5.nextLine();

        System.out.println("String: " + c5);
        System.out.println("Double: " + b5);
        System.out.println("Int: " + a5);
        
        input5.close();
	}
}