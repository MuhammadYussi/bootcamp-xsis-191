package hackerRank.Java;
import java.util.Scanner;

public class Java1DArray2 {
	static Scanner scan2;
	public static void main(String[] args) {
		Scanner scan2 = new Scanner(System.in);
		System.out.print("Total : ");
        int a2 = scan2.nextInt();
        int[] b2 = new int[a2];
        for(int j=0; j<a2; j++){
        	System.out.print("Nilai "+j+" : ");
        	b2[j] = scan2.nextInt();
        }
        scan2.close();

        for (int j = 0; j < b2.length; j++) {
            System.out.print(b2[j]+" ");
        }
	}
}
