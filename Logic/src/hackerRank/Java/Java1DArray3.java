package hackerRank.Java;
import java.util.Scanner;

public class Java1DArray3 {
	static Scanner scan3;
	public static void main(String[] args) {
		Scanner scan3 = new Scanner(System.in);
		System.out.print("Total : ");
        int a3 = scan3.nextInt();
        int[] b3 = new int[a3];
        for(int k=0; k<a3; k++){
        	System.out.print("Nilai "+k+" : ");
        	b3[k] = scan3.nextInt();
        }
        scan3.close();

        for (int k=0; k<b3.length; k++) {
            System.out.print(b3[k]+" ");
        }
	}
}
