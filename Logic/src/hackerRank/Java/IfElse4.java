package hackerRank.Java;
import java.util.Scanner;

public class IfElse4 {
	static Scanner input4;

    public static void main(String[] args) {
    	input4 = new Scanner(System.in);
        int d = input4.nextInt();
        if(d%2==1){
            System.out.println("Weird");
        }else{
            if(d>=2 && d<5){
                System.out.println("Not Weird");
            }else if(d>=6 && d<=20){
                System.out.println("Weird");
            }else if(d>20){
                System.out.println("Not Weird");
            }
        }

        input4.close();
    }
}
