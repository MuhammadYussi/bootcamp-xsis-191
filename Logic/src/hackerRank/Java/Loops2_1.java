package hackerRank.Java;
import java.util.Scanner;

public class Loops2_1 {
	static Scanner input1;
	public static void main(String []argh){
        Scanner input1 = new Scanner(System.in);
        System.out.print("Input : ");
        int q1=input1.nextInt();				//input jumlah permintaan untuk menghitung nilai
        for(int i=0;i<q1;i++){
        	System.out.print("Nilai Awal : ");
            int a1 = input1.nextInt();			//nilai awal
            System.out.print("Nilai Pengkali : ");
            int b1 = input1.nextInt();			//nilai pengkali
            System.out.print("Total Perhitungan : ");
            int n1 = input1.nextInt();			//banyaknya perhitungan tergantung pada n1

            for(int j=0; j<n1; j++){			//perulangan total nilai yang di output
                a1 = a1+b1;						//perhitungan utama
                System.out.print(a1+" ");		//output
                b1 = b1*2;						//perhitungan pangkat 2 untuk nilai pengkali
            }
            System.out.println();				//baris baru setiap permintaan
        }
        input1.close();
    }
}
