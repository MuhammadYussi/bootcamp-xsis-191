package hackerRank.Java;
import java.util.Scanner;

public class Java1DArray5 {
	static Scanner scan5;
	public static void main(String[] args) {
		Scanner scan5 = new Scanner(System.in);
		System.out.print("Total : ");
        int a5 = scan5.nextInt();
        int[] b5 = new int[a5];
        for(int m=0; m<a5; m++){
        	System.out.print("Nilai "+m+" : ");
        	b5[m] = scan5.nextInt();
        }
        scan5.close();

        for (int m=0; m<b5.length; m++) {
            System.out.print(b5[m]+" ");
        }
	}
}
