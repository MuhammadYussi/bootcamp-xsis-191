package hackerRank.Java;
import java.util.Scanner;

public class Java1DArray1 {
	static Scanner scan1;
	public static void main(String[] args) {
		Scanner scan1 = new Scanner(System.in);		//scan1 sebagai inputan
		System.out.print("Total : ");
        int a1 = scan1.nextInt();					//variable a1 berisi data bertipe int
        int[] b1 = new int[a1];						//nilai array b1 akan bernilai sama dengan a1
        for(int i=0; i<a1; i++){					//Perulangan input data array
        	System.out.print("Nilai "+i+" : ");
        	b1[i] = scan1.nextInt();				//nilai b1 yang berada pada index i akan diisi nilai baru
        }
        scan1.close();

        for (int i = 0; i < b1.length; i++) {		//perulangan untuk menampilkan array yang telah diisi
            System.out.print(b1[i]+" ");
        }
	}
}
