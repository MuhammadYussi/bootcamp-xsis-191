package hackerRank.Java;
import java.util.Scanner;

public class StdinStdout2_3 {
	static Scanner input3;
	public static void main(String[] args) {
		Scanner input3 = new Scanner(System.in);
		System.out.print("INT : ");
        int a3 = input3.nextInt();
        System.out.print("Double : ");
        Double b3 = input3.nextDouble();
        System.out.print("String : ");
        String c3 = input3.nextLine();

        System.out.println("String: " + c3);
        System.out.println("Double: " + b3);
        System.out.println("Int: " + a3);
        
        input3.close();
	}
}