package hackerRank.Java;
import java.util.Scanner;

public class StdinStdout1_3 {
	static Scanner scan3;
	public static void main(String[] args) {
        Scanner scan3 = new Scanner(System.in);
        System.out.print("Input 1 : ");
        int a3 = scan3.nextInt();
        System.out.print("Input 2 : ");
        int b3 = scan3.nextInt();
        System.out.print("Input 3 : ");
        int c3 = scan3.nextInt();

        System.out.println("");
        System.out.println(a3);
        System.out.println(b3);
        System.out.println(c3);
        scan3.close();
	}
}