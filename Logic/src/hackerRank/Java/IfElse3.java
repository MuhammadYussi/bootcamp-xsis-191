package hackerRank.Java;

import java.util.Scanner;

public class IfElse3 {
	static Scanner input3;

    public static void main(String[] args) {
    	input3 = new Scanner(System.in);
        int c = input3.nextInt();
        if(c%2==1){
            System.out.println("Weird");
        }else{
            if(c>=2 && c<5){
                System.out.println("Not Weird");
            }else if(c>=6 && c<=20){
                System.out.println("Weird");
            }else if(c>20){
                System.out.println("Not Weird");
            }
        }

        input3.close();
    }
}
