package hackerRank.Java;
import java.util.Scanner;

public class Loops2_3 {
	static Scanner input3;
	public static void main(String []argh){
        Scanner input3 = new Scanner(System.in);
        System.out.print("Input : ");
        int q3=input3.nextInt();
        for(int m=0;m<q3;m++){
        	System.out.print("Nilai Awal : ");
            int a3 = input3.nextInt();
            System.out.print("Nilai Pengkali : ");
            int b3 = input3.nextInt();
            System.out.print("Total Perhitungan : ");
            int n3 = input3.nextInt();

            for(int n=0; n<n3; n++){
                a3 = a3+b3;
                System.out.print(a3+" ");
                b3 = b3*2;
            }
            System.out.println();
        }
        input3.close();
    }
}
