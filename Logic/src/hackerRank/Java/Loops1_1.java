package hackerRank.Java;
import java.util.Scanner;

public class Loops1_1 {
	static Scanner input1;
	public static void main(String[] args) {
		input1 = new Scanner(System.in);
		System.out.print("Input : ");
        int a = input1.nextInt();
        
        for(int i=1; i<=10; i++){						//kondisi ketika i tidak memenuhi kriteria maka i akan diulang hingga memenuhi kriteria
            int result = a*i;							//hasil dari nilai N*i akan disimpan di variable result
            System.out.println(a+" x "+i+" = "+result);	//output result
        }
        input1.close();
    }
}
