package hackerRank.Java;
import java.util.Scanner;

public class IfElse2 {
	static Scanner input2;

    public static void main(String[] args) {
    	input2 = new Scanner(System.in);
        int b = input2.nextInt();
        if(b%2==1){
            System.out.println("Weird");
        }else{
            if(b>=2 && b<5){
                System.out.println("Not Weird");
            }else if(b>=6 && b<=20){
                System.out.println("Weird");
            }else if(b>20){
                System.out.println("Not Weird");
            }
        }

        input2.close();
    }
}
