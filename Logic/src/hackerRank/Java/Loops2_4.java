package hackerRank.Java;
import java.util.Scanner;

public class Loops2_4 {
	static Scanner input4;
	public static void main(String []argh){
        Scanner input4 = new Scanner(System.in);
        System.out.print("Input : ");
        int q4=input4.nextInt();
        for(int o=0;o<q4;o++){
        	System.out.print("Nilai Awal : ");
            int a4 = input4.nextInt();
            System.out.print("Nilai Pengkali : ");
            int b4 = input4.nextInt();
            System.out.print("Total Perhitungan : ");
            int n4 = input4.nextInt();

            for(int p=0; p<n4; p++){
                a4 = a4+b4;
                System.out.print(a4+" ");
                b4 = b4*2;
            }
            System.out.println();
        }
        input4.close();
    }
}
