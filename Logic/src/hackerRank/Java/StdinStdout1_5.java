package hackerRank.Java;
import java.util.Scanner;

public class StdinStdout1_5 {
	static Scanner scan5;
	public static void main(String[] args) {
        Scanner scan5 = new Scanner(System.in);
        System.out.print("Input 1 : ");
        int a5 = scan5.nextInt();
        System.out.print("Input 2 : ");
        int b5 = scan5.nextInt();
        System.out.print("Input 3 : ");
        int c5 = scan5.nextInt();

        System.out.println("");
        System.out.println(a5);
        System.out.println(b5);
        System.out.println(c5);
        scan5.close();
	}
}