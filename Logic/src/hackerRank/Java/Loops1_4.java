package hackerRank.Java;
import java.util.Scanner;

public class Loops1_4 {
	static Scanner input4;
	public static void main(String[] args) {
		input4 = new Scanner(System.in);
		System.out.print("Input : ");
        int d = input4.nextInt();
        
        for(int l=1; l<=10; l++){
            int result = d*l;
            System.out.println(d+" x "+l+" = "+result);
        }
        input4.close();
    }
}
