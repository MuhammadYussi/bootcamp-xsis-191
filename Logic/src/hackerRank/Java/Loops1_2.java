package hackerRank.Java;
import java.util.Scanner;

public class Loops1_2 {
	static Scanner input2;
	public static void main(String[] args) {
		input2 = new Scanner(System.in);
		System.out.print("Input : ");
        int b = input2.nextInt();
        
        for(int j=1; j<=10; j++){
            int result = b*j;
            System.out.println(b+" x "+j+" = "+result);
        }
        input2.close();
    }
}
