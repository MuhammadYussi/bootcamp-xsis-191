package hackerRank.Java;

import java.util.HashMap;
import java.util.Map;
import java.util.Scanner;

public class JavaAnagram {
	public static void main(String[] args) {
		Scanner scan = new Scanner(System.in);
        String a = scan.next();
        String b = scan.next();
        scan.close();
        boolean ret = isAnagram(a, b);
        System.out.println( (ret) ? "Anagrams" : "Not Anagrams" );
	}
	
	public static boolean isAnagram(String a, String b) {
		Map<String, Integer> map = new HashMap();
		String[] a1 = a.toLowerCase().split("");
		String[] b1 = b.toLowerCase().split("");
		for(int i=0; i<a1.length;i++) {
			if(map.containsKey(a1[i])) {
				int n = map.get(a1[i]);
				n++;
				map.put(a1[i], n);
			} else {
				map.put(a1[i], 1);
			}
		}
		for(int i=0; i<b1.length;i++) {
			if(!map.containsKey(b1[i])) {
				return false;
			}
			
			int n = map.get(b1[i]);
			if(n == 0) {
				return false;
			}else {
				n--;
				map.put(b1[i], n);
			}
		}
		
		return true;
	}
}
