package hackerRank.Java;
import java.util.Scanner;

public class StdinStdout1_4 {
	static Scanner scan4;
	public static void main(String[] args) {
        Scanner scan4 = new Scanner(System.in);
        System.out.print("Input 1 : ");
        int a4 = scan4.nextInt();
        System.out.print("Input 2 : ");
        int b4 = scan4.nextInt();
        System.out.print("Input 3 : ");
        int c4 = scan4.nextInt();

        System.out.println("");
        System.out.println(a4);
        System.out.println(b4);
        System.out.println(c4);
        scan4.close();
	}
}