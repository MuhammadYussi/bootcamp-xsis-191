package hackerRank.Java;
import java.util.Scanner;

public class Loops1_5 {
	static Scanner input5;
	public static void main(String[] args) {
		input5 = new Scanner(System.in);
		System.out.print("Input : ");
        int e = input5.nextInt();
        
        for(int m=1; m<=10; m++){
            int result = e*m;
            System.out.println(e+" x "+m+" = "+result);
        }
        input5.close();
    }
}
