package hackerRank.Java;
import java.util.Scanner;

public class StdinStdout2_4 {
	static Scanner input4;
	public static void main(String[] args) {
		Scanner input4 = new Scanner(System.in);
		System.out.print("INT : ");
        int a4 = input4.nextInt();
        System.out.print("Double : ");
        Double b4 = input4.nextDouble();
        System.out.print("String : ");
        String c4 = input4.nextLine();

        System.out.println("String: " + c4);
        System.out.println("Double: " + b4);
        System.out.println("Int: " + a4);
        
        input4.close();
	}
}