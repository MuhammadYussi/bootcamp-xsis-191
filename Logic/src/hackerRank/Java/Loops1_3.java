package hackerRank.Java;
import java.util.Scanner;

public class Loops1_3 {
	static Scanner input3;
	public static void main(String[] args) {
		input3 = new Scanner(System.in);
		System.out.print("Input : ");
        int c = input3.nextInt();
        
        for(int k=1; k<=10; k++){
            int result = c*k;
            System.out.println(c+" x "+k+" = "+result);
        }
        input3.close();
    }
}
