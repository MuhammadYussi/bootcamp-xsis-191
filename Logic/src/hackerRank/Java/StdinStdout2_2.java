package hackerRank.Java;
import java.util.Scanner;

public class StdinStdout2_2 {
	static Scanner input2;
	public static void main(String[] args) {
		Scanner input2 = new Scanner(System.in);
		System.out.print("INT : ");
        int a2 = input2.nextInt();
        System.out.print("Double : ");
        Double b2 = input2.nextDouble();
        System.out.print("String : ");
        String c2 = input2.nextLine();

        System.out.println("String: " + c2);
        System.out.println("Double: " + b2);
        System.out.println("Int: " + a2);
        
        input2.close();
	}
}