package hackerRank.Java;
import java.util.Scanner;

public class Loops2_5 {
	static Scanner input5;
	public static void main(String []argh){
        Scanner input5 = new Scanner(System.in);
        System.out.print("Input : ");
        int q5=input5.nextInt();
        for(int q=0;q<q5;q++){
        	System.out.print("Nilai Awal : ");
            int a5 = input5.nextInt();
            System.out.print("Nilai Pengkali : ");
            int b5 = input5.nextInt();
            System.out.print("Total Perhitungan : ");
            int n5 = input5.nextInt();

            for(int r=0; r<n5; r++){
                a5 = a5+b5;
                System.out.print(a5+" ");
                b5 = b5*2;
            }
            System.out.println();
        }
        input5.close();
    }
}
