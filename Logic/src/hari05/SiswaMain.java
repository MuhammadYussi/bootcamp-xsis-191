package hari05;

public class SiswaMain {
	public static void main(String[] args) {
		Siswa siswa = new Siswa(1, "Budi", "BatuRaden", "Pria", 9);
		System.out.println("Data Siswa 1 : ");
		siswa.output();
		
		Siswa siswa1 = new Siswa(1, "Rita", "Bukittinggi", "Wanita", 9);
		System.out.println("Data Siswa 2 : ");
		siswa1.output();
	}
}
