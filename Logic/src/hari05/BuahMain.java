package hari05;

public class BuahMain {
	public static void main(String[] args) {
		
		Buah fruit1 = new Buah("Nanas", "Kuning", "Asam", "Sedang");
		System.out.println("Kriteria Buah1\t:\t");
		fruit1.Output();
		
		Buah fruit2 = new Buah("Semangka", "Hijau", "Manis", "Besar");
		System.out.println("Kriteria Buah2\t:\t");
		fruit2.Output();
		
		Buah fruit3 = fruit1;
		fruit3.nama = "Pisang";
		fruit3.rasa = "Manis";
		System.out.println("Kriteria Buah3\t:\t");
		fruit3.Output();
		
		Buah fruit4 = new Buah("Strawberry", "Merah");
		System.out.println("Kriteria Buah4\t:\t");
		fruit4.Output();
		
		Buah fruit5 = fruit4;
		fruit5.nama = "Cherry";
		System.out.println("kriteria Buah 5\t:\t");
		fruit5.Output();
		
	}
}
