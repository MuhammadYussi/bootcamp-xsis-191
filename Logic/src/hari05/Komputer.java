package hari05;

public class Komputer {
	String prosessor;
	String vga;
	int ram;
	String hdd;
	String merek;
	
	public Komputer(String prosessor, String vga, int ram, String hdd, String merek){
		this.prosessor = prosessor;
		this.vga = vga;
		this.ram = ram;
		this.hdd = hdd;
		this.merek = merek;
	}
	
	public void output() {
		System.out.println("Prosessor\t\t: "+this.prosessor);
		System.out.println("VGA\t\t: "+this.vga);
		System.out.println("RAM\t\t: "+this.ram);
		System.out.println("Harddisk\t\t: "+this.hdd);
		System.out.println("Merek\t: "+this.merek);
		System.out.println("");
	}
}
