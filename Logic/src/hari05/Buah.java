package hari05;

public class Buah {
	String nama;
	String warna;
	String rasa;
	String ukuran;
	
	public Buah(String nama, String warna, String rasa, String ukuran) {
		this.nama = nama;
		this.warna = warna;
		this.rasa = rasa;
		this.ukuran = ukuran;
	}
	
	public Buah(String nama, String warna) {
		this.nama = nama;
		this.warna = warna;
	}
	
	public void Output() {
		System.out.println("Nama\t:\t"+this.nama);
		System.out.println("warna\t:\t"+this.warna);
		System.out.println("rasa\t:\t"+this.rasa);
		System.out.println("ukuran\t:\t"+this.ukuran);
	}
}
