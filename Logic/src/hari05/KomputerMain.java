package hari05;

public class KomputerMain {
	public static void main(String[] args) {
		Komputer comp1 = new Komputer("Intel core i5", "Nvidia 980mx", 8, "Toshiba", "acer");
		System.out.println("Data Komputer 1 : ");
		comp1.output();
		
		Komputer comp2 = new Komputer("Intel core i8", "Nvidia 1060mx", 16, "Seagate", "ASUS");
		System.out.println("Data Komputer 2 : ");
		comp2.output();
	}
}
