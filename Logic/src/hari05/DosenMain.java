package hari05;

public class DosenMain {
	public static void main(String[] args) {
		Dosen dosen = new Dosen(1, "Ratna", "Tangerang", "Wanita", "Bahasa", "Komunikasi");
		System.out.println("Data Dosen 1 : ");
		dosen.output();
		
		Dosen dosen1 = new Dosen(2, "Siti", "Pamulang", "Wanita", "Pemrograman","Sistem Informasi");
		System.out.println("Data Dosen 2 : ");
		dosen1.output();
	}
}
