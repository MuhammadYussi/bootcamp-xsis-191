package hari05;

public class Angkutan {
	String jurusan;
	String warna;
	int nomor;
	
	public Angkutan(String jurusan, String warna, int nomor){
		this.jurusan = jurusan;
		this.warna = warna;
		this.nomor = nomor;
	}
	
	public void output() {
		System.out.println("Jurusan\t\t: "+this.jurusan);
		System.out.println("Warna\t\t: "+this.warna);
		System.out.println("nomor\t\t: "+this.nomor);
		System.out.println("");
	}
}
