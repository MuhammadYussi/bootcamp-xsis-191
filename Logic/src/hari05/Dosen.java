package hari05;

public class Dosen {
	int id;
	String nama;
	String alamat;
	String jk;
	String matkul;
	String jurusan;
	
	public Dosen(int id, String nama, String alamat, String jk, String matkul, String jurusan) {
		this.id = id;
		this.nama = nama;
		this.alamat = alamat;
		this.jk = jk;
		this.matkul = matkul;
		this.jurusan = jurusan;
	}
	
	public void output() {
		System.out.println("ID\t\t: "+this.id);
		System.out.println("Nama\t\t: "+this.nama);
		System.out.println("Alamat\t\t: "+this.alamat);
		System.out.println("Gender\t\t: "+this.jk);
		System.out.println("Mata Kuliah\t: "+this.matkul);
		System.out.println("Jurusan\t\t: "+this.jurusan);
		System.out.println("");
	}
}
