package hari05;

public class OrangMain {

	public static void main(String[] args) {
		Orang org1 = new Orang(1, "Muhammad Yussi", "Yogyakarta", "Pria", 23);
		System.out.println("Data Orang 1:\t");
		org1.showData();
		
		Orang org2 = new Orang(2, "Yussi", "Yogyakarta");	//contoh instansiasi
		System.out.println("Data Orang 2:\t");
		org2.showData();
		
		Orang org3 = new Orang();
		System.out.println("Data Orang 3:\t");
		org3.showData();
		
		Orang org4 = org1;
		System.out.println("Data Orang 4:\t");
		org4.nama = "Bunga";
		org4.showData();
		System.out.println("Data Orang 1:\t");
		org1.showData();
		
		Orang org5 = org4;
		System.out.println("Data Orang 5:\t");
		org5.jk = "Tidak Diketahui";
		org5.showData();
		
	}

}
