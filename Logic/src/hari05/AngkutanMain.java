package hari05;

public class AngkutanMain {
	public static void main(String[] args) {
		Angkutan angkot = new Angkutan("lebak bulus", "biru", 106);
		System.out.println("Data Angkutan 1 : ");
		angkot.output();
		
		Angkutan angkot1 = new Angkutan("ciputat", "biru", 29);
		System.out.println("Data Angkutan 2 : ");
		angkot1.output();
		
		Angkutan angkot2 = new Angkutan("pamulang", "putih", 105);
		System.out.println("Data Angkutan 3 : ");
		angkot2.output();
	}
}
