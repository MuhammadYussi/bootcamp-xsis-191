package hari05;

public class Orang {
	//properti
	int id;
	String nama;
	String alamat;
	String jk;
	int umur;
	
	//constructor1
	public Orang() {
	}
	
	//constructor2
	public Orang(int id, String nama, String alamat, String jk, int umur) {		//ada 5 parameter
		this.id = id;
		this.nama = nama;
		this.alamat = alamat;
		this.jk = jk;
		this.umur = umur;
	}
	
	//contructor3
	public Orang(int id, String nama, String alamat) {
		this.id = id;
		this.nama = nama;
		this.alamat = alamat;
	}
	
	//method output
	public void showData() {
		System.out.println("ID\t:\t"+this.id);
		System.out.println("Nama\t:\t"+this.nama);
		System.out.println("Alamat\t:\t"+this.alamat);
		System.out.println("JK\t:\t"+this.jk);
		System.out.println("Umur\t:\t"+this.umur);
	}
}
