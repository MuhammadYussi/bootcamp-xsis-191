package hari05;

public class Siswa {
	int id;
	String nama;
	String alamat;
	String jk;
	int kelas;
	
	public Siswa(int id, String nama, String alamat, String jk, int kelas) {
		this.id = id;
		this.nama = nama;
		this.alamat = alamat;
		this.jk = jk;
		this.kelas = kelas;
	}
	
	public void output() {
		System.out.println("ID\t: "+this.id);
		System.out.println("Nama\t: "+this.nama);
		System.out.println("Alamat\t: "+this.alamat);
		System.out.println("Gender\t: "+this.jk);
		System.out.println("Kelas\t: "+this.kelas);
		System.out.println("");
	}
}
