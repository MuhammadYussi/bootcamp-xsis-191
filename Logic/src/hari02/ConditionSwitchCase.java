package hari02;

public class ConditionSwitchCase {
	public static void main(String[] args) {
		char grade = 'A';
		switch(grade) {
		case 'A':
			System.out.println("Excellent!");
			break;
		case 'B':
			System.out.println("Perfect!");
			break;
		case 'C':
			System.out.println("Great");
			break;
		case 'D':
			System.out.println("Cool~");
			break;
		case 'E':
			System.out.println("Bad...");
			break;
		case 'F':
			System.out.println("Miss");
			break;
		default:
			System.out.println("Invalid Rank");
		}
	}
}
