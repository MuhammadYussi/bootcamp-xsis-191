package hari02;

import java.util.Scanner;

public class MethodNonReturnType {
	static Scanner input;
	public static void main(String[] args) {
		//static
//		double point = 68.8;
//		methodRankPoints(point);
		
		//Dynamic
		input = new Scanner(System.in);
		System.out.print("Input Quality Score : ");
		double point = input.nextDouble();
		methodRankPoints(point);
	}
	
	public static void methodRankPoints(double point) {
		if(point <= 30.5) {
			System.out.println("Bad Quality");
		}else if( point <= 75.5) {
			System.out.println("Standard Quality");
		}else {
			System.out.println("Good Quality");
		}
	}
}
