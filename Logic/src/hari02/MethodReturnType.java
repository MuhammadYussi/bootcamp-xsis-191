package hari02;

import java.util.Scanner;

public class MethodReturnType {
	static Scanner input;
	public static void main(String[] args) {
		//Return 01 (static)
		int a = 11;
		int b = 6;
		int c = minFunction(a,b);						//tipe data a dan b harus sama dengan tipe data yang ada di statment 'minFunction'
		System.out.print("Minimum Value = "+c);
		
		//Return 02 (Dynamic)
		input = new Scanner(System.in);
		System.out.print("Input Number a : ");
		int d = input.nextInt();
		System.out.print("Input Number b : ");
		int e = input.nextInt();
		int f = maxFunction(d,e);
		System.out.print("Maximum Value = "+f);
	}
	
	public static int minFunction(int n1, int n2) {
		int min;
		if(n1>n2) {
			min = n2;
		}else {
			min = n1;
		}
		return min;										//fungsi untuk mengembalikan nilai 'minFunction' ketika dipanggil di Fungsi utama
	}
	
	public static int maxFunction(int n3,int n4) {
		int max;
		if(n3>n4) {
			max = n3;
		}else {
			max = n4;
		}
		return max;
	}
}
