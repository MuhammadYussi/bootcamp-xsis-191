package hari02;

import java.util.Scanner;

public class TugasLogic02 {
	static Scanner scn;

	public static void main(String[] args) {
		soal5();
	}

//===============================================================================================

	static void soal1() {
		
		//Deklarasi Var
		scn = new Scanner(System.in);
		System.out.print("N1 : ");
		int n1 = scn.nextInt();
		System.out.print("N2 : ");
		int n2 = scn.nextInt();
		int c = 1;
		int d = 0;
		int[] arr = new int[3];
		
		//index
		for(int n=0; n<n1; n++) {
			System.out.print(n + " ");
		}
		
		//New Line
		System.out.println("");
		
		//hasil deret angka
		for(int n=0; n<n1; n++) {
			System.out.print(c + " ");
			if(n < 3) {
				d = d+c;
			}
			//penambahan
			if(arr[0]==0) {
				arr[0] = c;
			}else if(arr[1]==0) {
				arr[1] = c;
			}else if(arr[2]==0) {
				arr[2] = c;
			}
			c = c*n2;
		}
		
		//output penambahan
		System.out.println("");
		System.out.print(arr[0] + "+" + arr[1] + "+" + arr[2] + " = ");
		System.out.println(d);
	}

//===============================================================================================

	static void soal2() {
		
		scn = new Scanner(System.in);
		System.out.print("Masukkan N1 : ");
		int n1 = scn.nextInt();
		System.out.print("Masukkan N2 : ");
		int n2 = scn.nextInt();
		int a = 1;
		int a1 = 0;
		int a2 = 0;
		int a3 = 0;
		
		for (int i = 0; i < n1; i++) {
			System.out.print(i+" ");
		}
		System.out.println(" ");
		
		for (int j = 0; j < n1; j++) {
			if(j == 0) {
				a1=a1+a;
			}
			else if(j == 1) {
				a=a*n2;
				a2=a2+a;
			}else if (j == 2) {
				a=a*-n2;
				a3=a3+a;
			}else if (j == 3) {
				a=a*-n2;
			}else if (j == 4) {
				a=a*n2;
			}else if (j == 5) {
				a=a*-n2;
			} else {
				a=a*-n2;
			}

			System.out.print(a+ " ");
		}

		int a4 = a1*a2*a3;
		System.out.println();
		System.out.print("Hasil Perhitungan : "+a1+" * "+a2+" * "+a3+" = " + a4);
	}

//===============================================================================================

	static void soal3() {
		
		//Deklarasi Var
		scn = new Scanner(System.in);
		System.out.print("N1 : ");
		int n1 = scn.nextInt();
		System.out.print("N2 : ");
		int n2 = scn.nextInt();
		System.out.print("N3 : ");
		int c = scn.nextInt();
		int d = 2;
		int total = 0;
		int[] arr= new int[3];
		
		//Index
		for(int i=0;i<n1;i++) {
			System.out.print(i+" ");
		}
		
		//New Line
		System.out.println(" ");
		
		//Perulangan
		for(int i=0;i<n1;i++) {
			System.out.print(d+" ");
			if(i==0) {
				d=d*n2;
			}else if(i==1 || i==2){
				d=d*c;
			}else if(i==3) {
				arr[0] = d;
				d=d/c;
			}else if(i==4) {
				arr[1] = d;
				d=d/c;
			}else if(i==5) {
				arr[2] = d;
				d=d/n2;
			}
		}
		
		//Pengurangan
		total = arr[0]-arr[1]-arr[2];
		System.out.println(" ");
		System.out.println(arr[0]+"-"+arr[1]+"-"+arr[2]+" = "+total);
	}

//===============================================================================================

	static void soal4() {
		
		//Deklarasi Var
		scn = new Scanner(System.in);
		System.out.print("N1 : ");
		int n1 = scn.nextInt();
		System.out.print("N2 : ");
		int n2 = scn.nextInt();
		int c = 1;
		int d = 5;
		int total = 0;
		int[] arr = new int [3];
		
		//Index
		for(int i=0; i<n1; i++) {
			System.out.print(i+" ");
		}
		
		//New Line
		System.out.println(" ");
		
		//Deret Angka
		for(int i=0; i<n1; i++) {
			if(i==0 || i==2 || i==4 || i==6) {
				System.out.print(c+" ");
				c=c+1;
			}else if(i==1) {
				arr[0] = d;
				System.out.print(d+" ");
				d=d+n2;
			}else if(i==3) {
				arr[1] = d;
				System.out.print(d+" ");
				d=d+n2;
			}else if(i==5) {
				arr[2] = d;
				System.out.print(d+" ");
				d=d+n2;
			}
		}
		
		//Penjumlahan
		total = arr[0]+arr[1]+arr[2];
		System.out.println("");
		System.out.println(arr[0]+"+"+arr[1]+"+"+arr[2]+" = "+total);
	}

//================================================================================================
	static void soal5() {
		scn = new Scanner(System.in);
		System.out.print("Text : ");
		String text = scn.nextLine();
		
		String[] array = text.split(" ");
		for(int i=0; i<array.length;i++) {
			String[] item = array[i].split("");
			for(int j=0; j<item.length;j++) {
				if(j>0 && j<item.length-1) {
					System.out.print("*");
				}else {
					System.out.print(item[j]);
				}
			}
			System.out.print(" ");
		}
	}
//================================================================================================

	static void soal6() {
	scn=new Scanner(System.in);
	System.out.print("Input Kalimat = ");
	String text=scn.nextLine();
	int uppercase=0;
	for(int i=0; i <text.length();i++) {
		if (Character.isUpperCase(text.charAt(i))) {
			uppercase++;
		}
	}
	
	System.out.println("");
	System.out.println(text);
	System.out.println("Total Uppercase letter : "+ uppercase);	
	}
}