package hari03;

import java.util.Scanner;

import common.PrintArray;

public class StudyCase1 {
	static Scanner scn;
	public static void main(String[] args) {
		scn = new Scanner(System.in);
		System.out.print("Masukkan N : ");
		int n = scn.nextInt();
		System.out.print("Masukkan M : ");
		int m = scn.nextInt();
		System.out.print("Masukkan O : ");
		int o = scn.nextInt();
		
		//1. buat array deret
		int[] deret = new int[n*4];
		int angka = o;
		for(int i=0;i<deret.length;i++) {
			if(i%4==3) {
				deret[i] = m;
			}else {
				deret[i] = angka;
				angka = angka+m;
			}
			//System.out.print(deret[i] + "\t"); //output 1 baris
		}
		
		//2. buat array 2 dimensi
		String[][] array = new String[n][n];
		
		//4. membuat index
		int index=0;
		
		//3. isi baris ke 0 dari kiri ke kanan
		for(int i=0; i<n; i++) {
			array[0][i]=deret[index]+" ";
			index++;
		}
		//3.1 isi kolom ke 6 dari atas ke bawah
		for(int i=1;i<n;i++) {
			array[i][6]=deret[index]+" ";
			index++;
		}
		//3.2 isi baris ke 6 dari kanan ke kiri
		for (int i=n-2; i>=0; i--) {
			array[n-1][i]=deret[index]+" ";
			index++;
		}
		//3.3 isi kolom ke 0 dari bawah ke atas
		for(int i=n-2;i>0; i--) {
			array[i][0]=deret[index]+" ";
			index++;
		}
		//output 2D menggunakan method dari class print array di package common
		PrintArray.array2D(array);
	}

}
