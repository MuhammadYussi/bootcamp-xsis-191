package hari03;

import java.util.Scanner;

import common.PrintArray;

public class StudyCase2Mirror {
	static Scanner scn;
	public static void main(String[] args) {
		
		//input
		scn = new Scanner(System.in);
		System.out.print("Masukkan Nilai N : ");
		int n = scn.nextInt();
		System.out.print("Masukkan Nilai M : ");
		int m = scn.nextInt();
		System.out.print("Masukkan Niai O : ");
		int o = scn.nextInt();
		
		//Deklarasi index, angka dan angka1
		int angka = o;
		int angka1 = m;
		
		//array deret
		int[] deret = new int[n*4];
		
		//deret angka
		for (int i = 0; i < deret.length; i++) {
			if(i%4==3) {
				deret[i] = angka1;
				angka1 = angka1*3;
			}else {
				deret[i] = angka;
				angka = angka+m;
			}
		}
		
		int index = n-1;
		
		//buat array 2D
		String[][] array = new String[n][n];
		// tampilan
		for (int i = n-1; i >=0; i--) {
			array[n-1-i][n-1-i]=deret[index]+"";
			index--;
		}
		
		for (int i = 1; i <n; i++) {
			array[i][0]=deret[index+n+1]+"";
			index++;
		}
		for (int i = 1; i < n-1; i++) {
			array[n-1][i]=deret[index+n+1]+"";
			index++;
		}
		
		//output array
		PrintArray.array2D(array);
	}
}
