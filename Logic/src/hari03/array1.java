package hari03;

public class array1 {
	public static void main(String[] args) {
		//deklarasi array cara 1
		int[] array = new int[10]; //index = 1-9
		
		//isi value element array
		array[0] = 10;
		array[1] = 9;
		array[2] = 8;
		array[3] = 7;
		array[4] = 6;
		array[5] = 5;
		array[6] = 4;
		array[7] = 3;
		array[8] = 2;
		array[9] = 1;
		//array[10] = 0;
		
		//output array
//		System.out.println(array.length);
//		for (int i = 0; i < array.length; i++) {
//			System.out.println(array[i]);
//		}
		
		//deklarasi array cara 2
		int[] array2 = new int[] {1,2,3,4,5};
		
		array2[0] = 9;
		array2[1] = 8;
		array2[2] = 7;
		array2[3] = 6;
		array2[4] = 5;
		
//		System.out.println(array2.length);
//		for (int i = 0; i < array2.length; i++) {
//			System.out.print(array[i] + " ");
//		}
	}
}
