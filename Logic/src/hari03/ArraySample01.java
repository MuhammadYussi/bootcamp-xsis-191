package hari03;

import java.util.Scanner;

public class ArraySample01 {

	static Scanner scn;
	public static void main(String[] args) {
		// 3 1 9 3 15 5 => N=6 (SOAL DERET)
		
		//deklarasi input + variable
		scn = new Scanner(System.in);
		System.out.print("Masukkan N : ");
		int n = scn.nextInt();
		int a = 3;
		int b = 1;
		
		//deklarasi array
		int[] array = new int[n];
		
		//perulangan deret angka
		for(int i=0; i<array.length; i++) {
			//mengisi array index genap dan ganjil
			if(i%2==0) { //index genap
				array[i] = a;
				a = a+6;
			}else { //index ganjil
				array[i] = b;
				b = b+2;
			}
			//output
			System.out.print(array[i]+"\t");
		}
	}

}
