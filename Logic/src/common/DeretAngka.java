package common;

public class DeretAngka {
	public static int[] deret01(int nPanjang, int nIncrement, int nAwal) {		//deret memiliki 3 parameter
		int[] deret = new int[nPanjang];			//mengatur array deret sama dengan nPanjang
		int angka = nAwal;
		for(int i = 0; i<deret.length;i++) {
			if(i%4==3) {
				deret[i] = nIncrement;				//deret[i] akan diisi nIncrement jika memenuhi kondisi
			}else {
				deret[i] = angka;					//jika tidak maka akan disi oleh angka
				angka = angka+nIncrement;
			}
		}
		
		return deret;								//mengembalikan nilai deret
	}
}
