package hari01;

import java.util.Scanner;

public class InputOutput {
	static Scanner input;
	public static void main(String[] args) {
		input = new Scanner(System.in);						//membuat input sebagai sebagai input baru
		//System.out.println("Masukkan Nama Anda\t: ");		//input dengan baris baru
		System.out.print("Masukkan Nama Anda\t: ");			//input biasa
		//String nama = input.next();						//yang akan dibaca hanya kata pertama
		String nama = input.nextLine();						//akan dibaca seluruhnya oleh variable nama
		System.out.println("Hello "+nama);
		input.close();										//menutup fungsi dari (system.in) 
	}
}
