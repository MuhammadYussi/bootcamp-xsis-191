package hari01;

public class TugasLogic01 {

	public static void main(String[] args) {
		//soal1();
		//soal2();
		//soal3();
		//soal4();
		soal5();
		//soal6();
		//soal7();
		//soal8();
		//soal9();
		//soal10();
	}
	
	public static void soal1(){
		int a = 1;
		for(int n=0; n<7; n++) {
			System.out.println(a);
			a=a+2;
		}
	}
	
	public static void soal2(){
		int a = 2;
		for(int n=0; n<7; n++) {
			System.out.println(a);
			a=a+2;
		}
	}
	
	public static void soal3() {
		int a = 1;
		for(int n=0; n<7; n++) {
			System.out.println(a);
			a=a+3;
		}
	}
	
	public static void soal4() {
		int a = 1;
		for(int n=0; n<7; n++) {
			System.out.println(a);
			a=a+3;
		}
	}
	
	public static void soal5() {
		int a = 1;
		for(int n=0; n<7; n++) {
			if(n == 2 || n == 5) {
				System.out.println("*");
			}else {
				System.out.println(a);
				a=a+4;
			}
		}
	}
	
	public static void soal6() {
		int a = 1;
		for(int n=0; n<7; n++) {
			if(n == 2 || n == 5) {
				System.out.println("*");
			}else {
				System.out.println(a);
			}
			a=a+4;
		}
	}
	
	public static void soal7() {
		int a = 2;
		for(int n=0; n<7; n++) {
			System.out.println(a);
			a=a*2;
		}
	}
	
	public static void soal8() {
		int a = 3;
		for(int n=0; n<7; n++) {
			System.out.println(a);
			a=a*3;
		}
	}
	
	public static void soal9() {
		int a = 4;
		for(int n=0; n<7; n++) {
			if(n == 2 || n == 5) {
				System.out.println("*");
			}else {
				System.out.println(a);
				a=a*4;
			}
		}
	}
	
	public static void soal10() {
		int a = 3;
		for(int n=0; n<7; n++) {
			if(n == 3) {
				System.out.println("xxx");
			}else {
				System.out.println(a);
			}
			a=a*3;
		}
	}
}
