package hari01;

public class HelloWorld {
	
	public static void main(String[] args) {
		System.out.println("welcome to java");
		System.out.println("");
		makananFavorit();
		System.out.println("");
		
		Orang or01 = new Orang();		//or01 adalah object yang berisi properti nama,alamat,dll
		or01.nama="Muhammad Yussi";
		or01.alamat="Kricak Kidul";
		or01.jk="Laki-Laki";
		or01.tmptLahir="Yogyakarta";
		or01.umur=23;
		or01.cetak();
		
		Orang or02 = new Orang();
		or02.nama="Muhammad";
		or02.alamat="Kricak";
		or02.jk="Laki-Laki";
		or02.tmptLahir="Jakarta";
		or02.umur=33;
		or02.cetak();
		
		Orang or03 = new Orang();
		or03.nama="Yussi";
		or03.alamat="Kidul";
		or03.jk="Perempuan";
		or03.tmptLahir="Yogyakarta";
		or03.umur=53;
		or03.cetak();
		
		Orang or04 = new Orang();
		or04.nama="Muham";
		or04.alamat="Kri";
		or04.jk="Laki-Laki";
		or04.tmptLahir="Jakarta";
		or04.umur=63;
		or04.cetak();
		
		Orang or05 = new Orang();
		or05.nama="Mad";
		or05.alamat="Cak";
		or05.jk="Laki-Laki";
		or05.tmptLahir="Yogyakarta";
		or05.umur=73;
		or05.cetak();
	}
	
	//ini metode
	public static void makananFavorit() {
		System.out.println("1. Ayam Goreng");
		System.out.println("2. Ayam Bakar");
		System.out.println("3. Ayam Madu");
		System.out.println("4. Ayam Panggang");
		System.out.println("5. Ayam POP");
	}
	//end

}
