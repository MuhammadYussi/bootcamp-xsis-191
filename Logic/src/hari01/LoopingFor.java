package hari01;

import java.util.Scanner;

public class LoopingFor {
	static Scanner input;
	public static void main(String[] args) {
		
		//Sample input 01
		input = new Scanner(System.in);
		System.out.print("Input your Level Up\t: ");
		int level = input.nextInt();
		for(int i=0;i<level;i++) {
			System.out.println("Level Up +"+i);
		}
		input.close();
		
		//sample input 02
//		input = new Scanner(System.in);
//		System.out.print("Input your Number\t: ");
//		int numb = Integer.parseInt(input.nextLine());
//		for(int j=0;j<numb;j++) {
//			System.out.println("Your Number is "+j);
//		}
//		input.close();
	}
}
