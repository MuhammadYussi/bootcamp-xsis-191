package hari01;

import java.util.Scanner;

public class Looping {
	//membuat object scn
	static Scanner scn;
	
	//method main
	public static void main(String[] args) {
		//instansiasi
		scn = new Scanner(System.in);
		//inputOutput();				//input output
		/* belajar return
		int a = 10;
		int b = 50;
		int hasil1 = tambah(a,b);
		int hasil2 = kurang(a,b);
		int hasil3 = kali(a,b);
		float hasil4 = bagi(a,b);
		System.out.println("hasil tambah " +hasil1);
		System.out.println("hasil tambah " +hasil2);
		System.out.println("hasil tambah " +hasil3);
		System.out.println("hasil tambah " +hasil4);
		*/
		//3 1 9 3 15 5 21 7 
		System.out.println("Masukkan Nilai n : ");
		int n = scn.nextInt();
		
		int a = 3; int b = 1;
		for(int i = 0; i<n; i++) {
			System.out.println(a);
			System.out.println(b);
			a=a+6; b=b+2;
		}
	}
	/* Method Return
	static int tambah(int x,int y) {
		return x+y;
	}
	static int kali(int x,int y) {
		return x*y;
	}
	static int kurang(int x,int y) {
		return x-y;	
	}
	static float bagi(float x,float y) {
		return x/y;
	}
	*/
	
	//Method input output
	static void inputOutput() {
		//output
		System.out.println("Masukkan Nama : ");
		//membuat variable
		String nama = scn.nextLine();
		//output variable
		System.out.println("\nNama : "+nama);
	}
}
