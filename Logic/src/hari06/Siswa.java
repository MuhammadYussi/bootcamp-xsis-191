package hari06;

public class Siswa extends Orang{
	public String jurusan;
	public double ipk;
	
	public Siswa(String nama,String alamat, int umur,String jurusan, double ipk) {
		this.nama = nama;
		this.alamat = alamat;
		this.umur = umur;
		this.jurusan = jurusan;
		this.ipk = ipk;
	}
	
	public void Output() {
		System.out.println("Nama = "+this.nama);
		System.out.println("Alamat = "+this.alamat);
		System.out.println("Umur = "+this.umur);
		System.out.println("jurusan = "+this.jurusan);
		System.out.println("ipk = "+this.ipk);
	}
}