package hari06;

public class Dosen extends Orang {		//meminta warisan parameter pada class Orang
	public String MataKuliah;
	public String fakultas;
	
	public Dosen(int id, String nama, String alamat, String jk, String MataKuliah, String fakultas) {
		super(id, nama, alamat, jk);
		this.MataKuliah = MataKuliah;
		this.fakultas = fakultas;
	}
	
	//Output
	public void showDosen() {
		System.out.println("ID\t\t: "+ super.id);
		System.out.println("Nama\t\t: "+ super.nama);
		System.out.println("Alamat\t\t: "+ super.alamat);
		System.out.println("Gender\t\t: "+ super.jk);
		System.out.println("Mata Kuliah\t: " + this.MataKuliah);
		System.out.println("Fakultas]\t: "+ this.fakultas);
	}
}
