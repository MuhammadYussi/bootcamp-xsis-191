package hari06;

public class staff extends Orang {
	public int divisi;
	public int gaji;
	
	public staff() {
		
	}
	
	public staff(int id, int divisi, String nama, String alamat, String jk, int gaji) {
		super(id, nama, alamat, jk);
		this.divisi = divisi;
		this.gaji = gaji;
	}
	
	public void outputStaff() {
		System.out.println("ID\t: "+super.id);
		System.out.println("Divisi\t: "+this.divisi);
		System.out.println("Nama\t: "+super.nama);
		System.out.println("Alamat\t: "+super.alamat);
		System.out.println("Gender\t: "+super.jk);
		System.out.println("Gaji\t: "+this.gaji);
	}

}