package hari06;

public class Orang {		//class Orang beserta Parameternya
	int id;
	String nama;
	String alamat;
	String jk;
	int umur;
	
	//Default Constructor
	public Orang() {
		
	}
	
	//Constructor
	public Orang(int id, String nama, String alamat, String jk) {
		this.id = id;
		this.nama = nama;
		this.alamat = alamat;
		this.jk = jk;
	}
}